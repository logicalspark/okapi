package net.sf.okapi.roundtrip.integration;

import net.sf.okapi.common.filters.FilterConfiguration;
import net.sf.okapi.common.filters.FilterConfigurationMapper;
import net.sf.okapi.common.filters.IFilter;
import net.sf.okapi.common.integration.EventRoundTripIT;
import net.sf.okapi.common.integration.FileComparator;
import net.sf.okapi.filters.archive.ArchiveFilter;
import net.sf.okapi.filters.archive.Parameters;
import net.sf.okapi.filters.tmx.TmxFilter;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.FileNotFoundException;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;

@RunWith(JUnit4.class)
public class RoundTripArchiveIT extends EventRoundTripIT {
	private static final String CONFIG_ID = "okf_archive";
	private static final String DIR_NAME = "/archive/";
	private static final List<String> EXTENSIONS = Arrays.asList(".archive", ".zip");
	private static final FilterConfigurationMapper FCM = initFilterConfigurationMapper();

	public RoundTripArchiveIT() {
		super(CONFIG_ID, DIR_NAME, EXTENSIONS, RoundTripArchiveIT::filterConstructor);
	}

	private static IFilter filterConstructor() {
		ArchiveFilter filter = new ArchiveFilter();
		filter.setFilterConfigurationMapper(FCM);
		Parameters params = new Parameters();
		params.setFileNames("*.xliff, *.tmx, *.xlf");
		params.setConfigIds("okf_xliff, okf_tmx, okf_xliff");
		filter.setParameters(params);
		return filter;
	}

	private static FilterConfigurationMapper initFilterConfigurationMapper() {
		final FilterConfigurationMapper result = new FilterConfigurationMapper();
		// Create configuration for tmx extension (if we need text units from tmx as well)
		try (TmxFilter tmxFilter = new TmxFilter()) {
			for (FilterConfiguration cfg : tmxFilter.getConfigurations()) {
				result.addConfiguration(cfg);
			}
		}
		return result;
	}

	@Test
	public void archiveFiles() throws FileNotFoundException, URISyntaxException {
		setSerializedOutput(false);
		realTestFiles(false, new FileComparator.EventComparator());
	}

	@Test
	public void archiveFilesSerialized() throws FileNotFoundException, URISyntaxException {
		setSerializedOutput(true);
		realTestFiles(CONFIG_ID, false, new FileComparator.EventComparator());
	}
}
