/*
 * =============================================================================
 * Copyright (C) 2010-2021 by the Okapi Framework contributors
 * -----------------------------------------------------------------------------
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =============================================================================
 */
package net.sf.okapi.filters.openxml;

import net.sf.okapi.common.LocaleId;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

interface DispersedTranslation {
    String namespace();
    String rawSource();
    void prepareFor(final LocaleId locale, final String encodedTarget);
    String encodedTargetOrRawSourceFor(final LocaleId locale);

    final class Default implements DispersedTranslation {
        private final String namespace;
        private final String rawSource;
        private final Map<LocaleId, String> encodedTargets;

        Default(final String namespace, final String rawSource) {
            this(namespace, rawSource, new HashMap<>());
        }

        Default(
            final String namespace,
            final String rawSource,
            final Map<LocaleId, String> encodedTargets
        ) {
            this.namespace = namespace;
            this.rawSource = rawSource;
            this.encodedTargets = encodedTargets;
        }

        @Override
        public String namespace() {
            return this.namespace;
        }

        @Override
        public String rawSource() {
            return this.rawSource;
        }

        @Override
        public void prepareFor(final LocaleId locale, final String encodedTarget) {
            this.encodedTargets.put(locale, encodedTarget);
        }

        @Override
        public String encodedTargetOrRawSourceFor(final LocaleId locale) {
            return this.encodedTargets.getOrDefault(locale, this.rawSource);
        }

        @Override
        public boolean equals(final Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            final Default another = (Default) o;
            return this.namespace.equals(another.namespace)
                && this.rawSource.equals(another.rawSource);
        }

        @Override
        public int hashCode() {
            return Objects.hash(this.namespace, this.rawSource);
        }
    }
}
