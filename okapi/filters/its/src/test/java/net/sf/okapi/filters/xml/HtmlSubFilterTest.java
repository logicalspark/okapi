package net.sf.okapi.filters.xml;

import net.sf.okapi.common.Event;
import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.Namespaces;
import net.sf.okapi.common.filters.FilterConfigurationMapper;
import net.sf.okapi.common.filters.FilterTestDriver;
import net.sf.okapi.common.filters.IFilterConfigurationMapper;
import net.sf.okapi.common.resource.ITextUnit;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.assertEquals;

public class HtmlSubFilterTest {

    private XMLFilter filter;
    private final LocaleId locEN = LocaleId.fromString("en");

    @Before
    public void setUp() {
        filter = new XMLFilter();
    }

    @Test
    public void testHtmlSubFilter() {
        String snippet = "<?xml version=\"1.0\"?>\n"
                + "<doc><its:rules version=\"1.0\" xmlns:its=\"http://www.w3.org/2005/11/its\" "
                + "xmlns:ix=\""+ Namespaces.ITSX_NS_URI+"\">"
                + "<ix:subFilterRule selector=\"//sf\" subFilter=\"okf_html\"/>"
                + "</its:rules>"
                + "<p>Text1</p>"
                + "<sf>Text2</sf>"
                + "<p>Text3</p>"
                + "</doc>";

        ArrayList<Event> list = getEvents(snippet);
        ITextUnit tu = FilterTestDriver.getTextUnit(list, 1);
        assertEquals("Text1", tu.getSource().toString());
        tu = FilterTestDriver.getTextUnit(list, 2);
        assertEquals("Text2", tu.getSource().toString());
        tu = FilterTestDriver.getTextUnit(list, 3);
        assertEquals("Text3", tu.getSource().toString());
    }

    @Test
    public void testHtmlSubFilter_Blank() {
        String snippet = "<?xml version=\"1.0\"?>\n"
                + "<doc><its:rules version=\"1.0\" xmlns:its=\"http://www.w3.org/2005/11/its\" "
                + "xmlns:ix=\""+ Namespaces.ITSX_NS_URI+"\">"
                + "<ix:subFilterRule selector=\"//sf\" subFilter=\"okf_html\"/>"
                + "</its:rules>"
                + "<p>Text1</p>"
                + "<sf></sf>"
                + "<p>Text3</p>"
                + "</doc>";

        ArrayList<Event> list = getEvents(snippet);
        ITextUnit tu = FilterTestDriver.getTextUnit(list, 1);
        assertEquals("Text1", tu.getSource().toString());
        tu = FilterTestDriver.getTextUnit(list, 2);
        assertEquals("Text3", tu.getSource().toString());
    }

    @Test
    public void testHtml5SubFilter() {
        String snippet = "<?xml version=\"1.0\"?>\n"
                + "<doc><its:rules version=\"1.0\" xmlns:its=\"http://www.w3.org/2005/11/its\" "
                + "xmlns:ix=\""+ Namespaces.ITSX_NS_URI+"\">"
                + "<ix:subFilterRule selector=\"//sf\" subFilter=\"okf_itshtml5\"/>"
                + "</its:rules>"
                + "<p>Text1</p>"
                + "<sf>Text2</sf>"
                + "<p>Text3</p>"
                + "</doc>";

        ArrayList<Event> list = getEvents(snippet);
        ITextUnit tu = FilterTestDriver.getTextUnit(list, 1);
        assertEquals("Text1", tu.getSource().toString());
        tu = FilterTestDriver.getTextUnit(list, 2);
        assertEquals("Text2", tu.getSource().toString());
        tu = FilterTestDriver.getTextUnit(list, 3);
        assertEquals("Text3", tu.getSource().toString());
    }

    @Test
    public void testHtml5SubFilter_Blank() {
        String snippet = "<?xml version=\"1.0\"?>\n"
                + "<doc><its:rules version=\"1.0\" xmlns:its=\"http://www.w3.org/2005/11/its\" "
                + "xmlns:ix=\""+ Namespaces.ITSX_NS_URI+"\">"
                + "<ix:subFilterRule selector=\"//sf\" subFilter=\"okf_itshtml5\"/>"
                + "</its:rules>"
                + "<p>Text1</p>"
                + "<sf></sf>"
                + "<p>Text3</p>"
                + "</doc>";

        ArrayList<Event> list = getEvents(snippet);
        ITextUnit tu = FilterTestDriver.getTextUnit(list, 1);
        assertEquals("Text1", tu.getSource().toString());
        tu = FilterTestDriver.getTextUnit(list, 2);
        assertEquals("Text3", tu.getSource().toString());
    }

    private ArrayList<Event> getEvents(String snippet) {
        IFilterConfigurationMapper fcMapper = new FilterConfigurationMapper();
        fcMapper.addConfigurations(XMLFilter.class.getCanonicalName());
        fcMapper.addConfigurations("net.sf.okapi.filters.html.HtmlFilter");
        fcMapper.addConfigurations("net.sf.okapi.filters.its.html5.HTML5Filter");
        filter.setFilterConfigurationMapper(fcMapper);
        return FilterTestDriver.getEvents(filter, snippet, locEN, LocaleId.FRENCH);
    }

}
