/*
 * =============================================================================
 * Copyright (C) 2010-2022 by the Okapi Framework contributors
 * -----------------------------------------------------------------------------
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =============================================================================
 */
package net.sf.okapi.filters.idml;

import net.sf.okapi.common.IResource;
import net.sf.okapi.common.ISkeleton;

import javax.xml.namespace.QName;
import java.util.Map;

class TextSkeleton implements ISkeleton {
    static final QName DEFAULT_ATTRIBUTE_NAME = new QName("");

    private final Element element;
    private final Map<Integer, MarkupRange> codeMap;
    private final QName attributeName;
    private IResource parent;

    TextSkeleton(
        final Element element,
        final Map<Integer, MarkupRange> codeMap
    ) {
        this(element, codeMap, TextSkeleton.DEFAULT_ATTRIBUTE_NAME);
    }

    TextSkeleton(
        final Element element,
        final Map<Integer, MarkupRange> codeMap,
        final QName attributeName
    ) {
        this.element = element;
        this.codeMap = codeMap;
        this.attributeName = attributeName;
    }

    Element element() {
        return this.element;
    }

    Map<Integer, MarkupRange> codeMap() {
        return this.codeMap;
    }

    QName attributeName() {
        return this.attributeName;
    }

    @Override
    public ISkeleton clone() {
        final TextSkeleton textSkeleton = new TextSkeleton(
            this.element,
            this.codeMap,
            this.attributeName
        );
        textSkeleton.setParent(this.parent);
        return textSkeleton;
    }

    @Override
    public void setParent(IResource parent) {
        this.parent = parent;
    }

    @Override
    public IResource getParent() {
        return parent;
    }
}
