echo "===== Building example01 ====="
if exist example01\target rd /q/s example01\target
md example01\target
javac -d example01/target example01/src/main/java/*.java -cp .;../lib/*
jar cf example01/target/okapi-example-01.jar -C example01/target .

echo "===== Building example02 ====="
if exist example02\target rd /q/s example02\target
md example02\target
javac -d example02/target example02/src/main/java/*.java -cp .;../lib/*
jar cf example02/target/okapi-example-02.jar -C example02/target .

echo "===== Building example03 ====="
if exist example03\target rd /q/s example03\target
md example03\target
copy example03\src\main\resources example03\target
javac -d example03/target example03/src/main/java/*.java -cp .;../lib/*
jar cf example03/target/okapi-example-03.jar -C example03/target .

echo "===== Building example04 ====="
if exist example04\target rd /q/s example04\target
md example04\target
javac -d example04/target example04/src/main/java/*.java -cp .;../lib/*
jar cf example04/target/okapi-example-04.jar -C example04/target .

echo "===== Building example05 ====="
if exist example05\target rd /q/s example05\target
md example05\target
javac -d example05/target example05/src/main/java/*.java -cp .;../lib/*
jar cf example05/target/okapi-example-05.jar -C example05/target .

echo "===== Building example06 ====="
if exist example06\target rd /q/s example06\target
md example06\target
javac -d example06/target example06/src/main/java/*.java -cp .;../lib/*
jar cf example06/target/okapi-example-06.jar -C example06/target .

echo "===== Building example07 ====="
if exist example07\target rd /q/s example07\target
md example07\target
copy example07\src\main\resources example07\target
javac -d example07/target example07/src/main/java/*.java -cp .;../lib/*
jar cf example07/target/okapi-example-07.jar -C example07/target .

pause
