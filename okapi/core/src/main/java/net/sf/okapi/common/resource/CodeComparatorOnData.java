/*===========================================================================
  Copyright (C) 2016-2017 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
===========================================================================*/

package net.sf.okapi.common.resource;

import net.sf.okapi.common.StringUtil;
import net.sf.okapi.common.Util;

import java.util.Comparator;

public class CodeComparatorOnData implements Comparator<Code> {

	@Override
	public int compare(Code c1, Code c2) {
		if (c1 == c2) {
			return 0;
		}

		if (c1 == null || c2 == null) {
			return -1;
		}

		// default is compare on Code#data
		String data1 = c1.getData();
		String data2 = c2.getData();

		// FIXME: remove whitespace because the XLIFF/TMX etc.. code data may be normalized
		data1 = StringUtil.removeWhiteSpace(data1);
		data2 = StringUtil.removeWhiteSpace(data2);
		return data2.compareTo(data1);
	}
}
