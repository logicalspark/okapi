/*
 * =============================================================================
 * Copyright (C) 2010-2022 by the Okapi Framework contributors
 * -----------------------------------------------------------------------------
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =============================================================================
 */
package net.sf.okapi.common.ui.filters;

import net.sf.okapi.common.filters.fontmappings.DefaultFontMapping;
import net.sf.okapi.common.filters.fontmappings.DefaultFontMappings;
import net.sf.okapi.common.filters.fontmappings.FontMappings;
import net.sf.okapi.common.ui.ResponsiveTable;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(JUnit4.class)
public class ResponsiveTableFontMappingsTest {
    @Test @Ignore
    public void constructed() {
        final ResourceBundle rb = ResourceBundle.getBundle("net.sf.okapi.common.ui.filters.ResponsiveTableFontMappingsOutput");
        final Shell shell = new Shell();
        final ResponsiveTable rt = new ResponsiveTable.Default(
            new Table(shell, SWT.MULTI | SWT.BORDER | SWT.FULL_SELECTION),
            new Menu(shell, SWT.POP_UP),
            new Menu(shell, SWT.POP_UP)
        );
        rt.configureHeader(
            new String[] {
                rb.getString("source-locale-pattern"),
                rb.getString("target-locale-pattern"),
                rb.getString("source-font-pattern"),
                rb.getString("target-font")
            }
        );
        rt.addRow(new String[] {});
        rt.addRow(new String[] {"", "", "Times.*", "Arial Unicode MS"});
        rt.addRow(new String[] {".*", ".*", "Arial", "Times New Roman"});
        final FontMappings fm = new DefaultFontMappings(new LinkedList<>());
        fm.addFrom(new ResponsiveTableFontMappingsInput(rt));
        assertThat(fm.targetFontFor("Absent Font")).isEqualTo("Absent Font");
        assertThat(fm.targetFontFor("Times")).isEqualTo("Arial Unicode MS");
        assertThat(fm.targetFontFor("Arial")).isEqualTo("Times New Roman");
        shell.close();
    }

    @Test @Ignore
    public void exposed() {
        final FontMappings fms = new DefaultFontMappings(
            new DefaultFontMapping("", "", "", ""),
            new DefaultFontMapping(
                "",
                "",
                "Times.*",
                "Arial Unicode MS"
            ),
            new DefaultFontMapping(
                ".*",
                ".*",
                "Arial",
                "Times New Roman"
            )
        );
        final Shell shell = new Shell();
        final Table table = new Table(shell, SWT.MULTI | SWT.BORDER | SWT.FULL_SELECTION);
        final ResponsiveTable rt = fms.writtenTo(
            new ResponsiveTableFontMappingsOutput(
                new ResponsiveTable.Default(
                    table,
                    new Menu(shell, SWT.POP_UP),
                    new Menu(shell, SWT.POP_UP)
                )
            )
        );
        final ResourceBundle rb = ResourceBundle.getBundle("net.sf.okapi.common.ui.filters.ResponsiveTableFontMappingsOutput");
        assertThat(
            tableColumnsToStringList(table.getColumns())
        ).isEqualTo(
            Arrays.asList(
                rb.getString("source-locale-pattern"),
                rb.getString("target-locale-pattern"),
                rb.getString("source-font-pattern"),
                rb.getString("target-font")
            )
        );
        final List<List<String>> output = tableItemTextsToLists(table.getColumnCount(), rt.rows());
        assertThat(
            output
        ).isEqualTo(
            Arrays.asList(
                Arrays.asList("", "", "", ""),
                Arrays.asList("", "", "Times.*", "Arial Unicode MS"),
                Arrays.asList(".*", ".*", "Arial", "Times New Roman")
            )
        );
        shell.close();
    }

    private List<String> tableColumnsToStringList(final TableColumn[] columns) {
        return Arrays.stream(columns)
            .map(tc -> tc.getText())
            .collect(Collectors.toList());
    }

    private static List<List<String>> tableItemTextsToLists(final int columnCount, final TableItem[] items) {
        return Arrays.stream(items)
            .map(ti -> tableItemToStringList(columnCount, ti))
            .collect(Collectors.toList());
    }

    private static List<String> tableItemToStringList(final int columnCount, final TableItem ti) {
        final List<String> strings = new LinkedList<>();
        for (int i = 0; i < columnCount; i++) {
            strings.add(ti.getText(i));
        }
        return strings;
    }
}
