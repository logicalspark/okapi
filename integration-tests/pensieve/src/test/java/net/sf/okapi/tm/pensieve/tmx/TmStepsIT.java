package net.sf.okapi.tm.pensieve.tmx;

import net.sf.okapi.common.*;
import net.sf.okapi.common.annotation.AltTranslation;
import net.sf.okapi.common.annotation.AltTranslationsAnnotation;
import net.sf.okapi.common.logger.EventLogger;
import net.sf.okapi.common.logger.TextUnitLogger;
import net.sf.okapi.common.pipelinebuilder.*;
import net.sf.okapi.common.query.MatchType;
import net.sf.okapi.common.resource.*;
import net.sf.okapi.filters.plaintext.PlainTextFilter;
import net.sf.okapi.lib.reporting.IReportGenerator;
import net.sf.okapi.steps.common.RawDocumentToFilterEventsStep;
import net.sf.okapi.steps.leveraging.LeveragingStep;
import net.sf.okapi.steps.scopingreport.Parameters;
import net.sf.okapi.steps.scopingreport.ScopingReportStep;
import net.sf.okapi.steps.segmentation.SegmentationStep;
import net.sf.okapi.steps.wordcount.CharacterCountStep;
import net.sf.okapi.steps.wordcount.WordCountStep;
import net.sf.okapi.steps.wordcount.categorized.gmx.GMXFuzzyMatchCharacterCountStep;
import net.sf.okapi.steps.wordcount.categorized.gmx.GMXFuzzyMatchWordCountStep;
import net.sf.okapi.steps.wordcount.categorized.gmx.GMXLeveragedMatchedCharacterCountStep;
import net.sf.okapi.steps.wordcount.categorized.gmx.GMXLeveragedMatchedWordCountStep;
import net.sf.okapi.steps.wordcount.common.BaseCountStep;
import net.sf.okapi.steps.wordcount.common.BaseCounter;
import net.sf.okapi.steps.wordcount.common.GMX;
import net.sf.okapi.tm.pensieve.common.TranslationUnit;
import net.sf.okapi.tm.pensieve.common.TranslationUnitVariant;
import net.sf.okapi.tm.pensieve.writer.ITmWriter;
import net.sf.okapi.tm.pensieve.writer.TmWriterFactory;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * This integrattion test class includes test cases that requires
 * a Pensive TM instance.
 * <br/>
 * The test cases were moved from these unit tests:
 * <pre>
 *  okapi/steps/wordcount/src/test/java/net/sf/okapi/steps/wordcount/categorized/gmx/TestGMXCounts.java
 *  okapi/steps/scopingreport/src/test/java/net/sf/okapi/steps/scopingreport/TestScopingReport.java
 *  okapi/steps/scopingreport/src/test/java/net/sf/okapi/steps/scopingreport/TestFields.java (deleted)
 * </pre>
 */
@RunWith(JUnit4.class)
public class TmStepsIT {

    static private final LocaleId locENUS = LocaleId.fromString("EN-US");
    static private final LocaleId locFRFR = LocaleId.fromString("FR-FR");
    static private final LocaleId locESES = LocaleId.fromString("ES-ES");

    static FileLocation root;
    static String dbDirectory;

    private BaseCountStep bcs;
    private StartDocument sd;
    private Event sdEvent;
    private ITextUnit tu;
    private Event tuEvent;
    private IReportGenerator gen;

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @BeforeClass
    public static void createTestTm() { // Copied from PensieveTMConnectorTest.java
        root = FileLocation.fromClass(TmStepsIT.class);
        dbDirectory = root.out("/testtm").toString() + File.separator;
        Util.createDirectories(dbDirectory);

        ITmWriter writer = TmWriterFactory.createFileBasedTmWriter(dbDirectory, true);

        // TU 1
        TranslationUnit tu = new TranslationUnit();
        TranslationUnitVariant tuv = new TranslationUnitVariant(locENUS,
                new TextFragment("Elephants cannot fly."));
        tu.setSource(tuv);
        tuv = new TranslationUnitVariant(locFRFR,
                new TextFragment("Les \u00e9l\u00e9phants ne peuvent pas voler."));
        tu.setTarget(tuv);
        writer.indexTranslationUnit(tu);

        // TU 2
        tu = new TranslationUnit();
        TextFragment tf = new TextFragment("Elephants ");
        tf.append(TextFragment.TagType.OPENING, "b", "<b>");
        tf.append("cannot");
        tf.append(TextFragment.TagType.CLOSING, "b", "</b>");
        tf.append(" fly.");
        tuv = new TranslationUnitVariant(locENUS, tf);
        tu.setSource(tuv);
        tf = new TextFragment("Les \u00e9l\u00e9phants ");
        tf.append(TextFragment.TagType.OPENING, "b", "<b>");
        tf.append("ne peuvent pas");
        tf.append(TextFragment.TagType.CLOSING, "b", "</b>");
        tf.append(" voler.");
        tuv = new TranslationUnitVariant(locFRFR, tf);
        tu.setTarget(tuv);
        writer.indexTranslationUnit(tu);

        // TU 3
        tu = new TranslationUnit();
        tf = new TextFragment("Elephants ");
        tf.append(TextFragment.TagType.OPENING, "g0", "<g0>");
        tf.append("cannot");
        tf.append(TextFragment.TagType.CLOSING, "g0", "</g0>");
        tf.append(" fly.");
        tuv = new TranslationUnitVariant(locENUS, tf);
        tu.setSource(tuv);
        tf = new TextFragment("Les \u00e9l\u00e9phants ");
        tf.append(TextFragment.TagType.OPENING, "g0", "<g0>");
        tf.append("ne peuvent pas");
        tf.append(TextFragment.TagType.CLOSING, "g0", "</g0>");
        tf.append(" voler.");
        tuv = new TranslationUnitVariant(locFRFR, tf);
        tu.setTarget(tuv);
        writer.indexTranslationUnit(tu);

        writer.close();
    }

    private static void testPath(String path) {
        Logger localLogger = LoggerFactory.getLogger(TmStepsIT.class); // loggers are cached
        localLogger.debug(new File(path).getAbsolutePath());
    }

    private void startupGmxTest() {
        sd = new StartDocument("sd");
        sd.setLocale(LocaleId.ENGLISH);
        sdEvent = new Event(EventType.START_DOCUMENT, sd);

        tu = new TextUnit("tu");
        tu.setSource(new TextContainer("12:00 is 15 minutes after 11:45. You can check at freetime@example.com 8-) for $300"));
        tuEvent = new Event(EventType.TEXT_UNIT, tu);
        root = FileLocation.fromClass(getClass());
    }

    // === Test cases below are from TestGMXCounts === //
    @Test
    public void testGMXExactMatchedWordCountStep () {
        startupGmxTest();
        net.sf.okapi.connectors.pensieve.Parameters rparams =
                new net.sf.okapi.connectors.pensieve.Parameters();
        rparams.setDbDirectory(dbDirectory);
        tu.setSource(new TextContainer("Elephants cannot fly."));

        LeveragingStep ls = new LeveragingStep();
        ls.setSourceLocale(LocaleId.ENGLISH);
        ls.setTargetLocale(LocaleId.FRENCH);
        net.sf.okapi.steps.leveraging.Parameters params = ls.getParameters();
        params.setResourceParameters(rparams.toString());
        params.setResourceClassName(net.sf.okapi.connectors.pensieve.PensieveTMConnector.class.getName());
        params.setThreshold(99);
        params.setFillTarget(true);
        ls.handleEvent(sdEvent);
        ls.handleEvent(tuEvent);
        logger.debug(TextUnitLogger.getTuInfo(tu, LocaleId.ENGLISH));
        AltTranslationsAnnotation ata = tu.getTarget(LocaleId.FRENCH).getAnnotation(AltTranslationsAnnotation.class);
        ata.add(new AltTranslation(LocaleId.ENGLISH, LocaleId.FRENCH, tu.getSource().getFirstContent(), null, null, MatchType.EXACT_UNIQUE_ID, 100, null));
        logger.debug(TextUnitLogger.getTuInfo(tu, LocaleId.ENGLISH));

        bcs = new GMXLeveragedMatchedWordCountStep();
        bcs.setSourceLocale(LocaleId.ENGLISH);
        bcs.setTargetLocale(LocaleId.FRENCH);
        bcs.handleEvent(sdEvent);
        bcs.handleEvent(tuEvent);
        logger.debug(TextUnitLogger.getTuInfo(tu, LocaleId.ENGLISH));

        assertEquals(3, BaseCounter.getCount(tu, GMX.LeveragedMatchedWordCount));
    }

    @Test
    public void testGMXExactMatchedCharacterCountStep () {
        startupGmxTest();
        net.sf.okapi.connectors.pensieve.Parameters rparams =
                new net.sf.okapi.connectors.pensieve.Parameters();
        rparams.setDbDirectory(dbDirectory);
        tu.setSource(new TextContainer("Elephants cannot fly."));

        LeveragingStep ls = new LeveragingStep();
        ls.setSourceLocale(LocaleId.ENGLISH);
        ls.setTargetLocale(LocaleId.FRENCH);
        net.sf.okapi.steps.leveraging.Parameters params = ls.getParameters();
        params.setResourceParameters(rparams.toString());
        params.setResourceClassName(net.sf.okapi.connectors.pensieve.PensieveTMConnector.class.getName());
        params.setThreshold(99);
        params.setFillTarget(true);
        ls.handleEvent(sdEvent);
        ls.handleEvent(tuEvent);
        logger.debug(TextUnitLogger.getTuInfo(tu, LocaleId.ENGLISH));
        AltTranslationsAnnotation ata = tu.getTarget(LocaleId.FRENCH).getAnnotation(AltTranslationsAnnotation.class);
        ata.add(new AltTranslation(LocaleId.ENGLISH, LocaleId.FRENCH, tu.getSource().getFirstContent(), null, null, MatchType.EXACT_UNIQUE_ID, 100, null));
        logger.debug(TextUnitLogger.getTuInfo(tu, LocaleId.ENGLISH));

        bcs = new GMXLeveragedMatchedCharacterCountStep();
        bcs.setSourceLocale(LocaleId.ENGLISH);
        bcs.setTargetLocale(LocaleId.FRENCH);
        bcs.handleEvent(sdEvent);
        bcs.handleEvent(tuEvent);
        logger.debug(TextUnitLogger.getTuInfo(tu, LocaleId.ENGLISH));

        assertEquals(18, BaseCounter.getCount(tu, GMX.LeveragedMatchedCharacterCount));
    }

    @Test
    public void testGMXLeveragedMatchedWordCountStep () {
        startupGmxTest();
        net.sf.okapi.connectors.pensieve.Parameters rparams =
                new net.sf.okapi.connectors.pensieve.Parameters();
        rparams.setDbDirectory(dbDirectory);
        tu.setSource(new TextContainer("Elephants cannot fly."));

        LeveragingStep ls = new LeveragingStep();
        ls.setSourceLocale(LocaleId.ENGLISH);
        ls.setTargetLocale(LocaleId.FRENCH);
        net.sf.okapi.steps.leveraging.Parameters params = ls.getParameters();
        params.setResourceParameters(rparams.toString());
        params.setResourceClassName(net.sf.okapi.connectors.pensieve.PensieveTMConnector.class.getName());
        params.setThreshold(99);
        params.setFillTarget(true);
        ls.handleEvent(sdEvent);
        ls.handleEvent(tuEvent);
        logger.debug(TextUnitLogger.getTuInfo(tu, LocaleId.ENGLISH));

        bcs = new GMXLeveragedMatchedWordCountStep();
        bcs.setSourceLocale(LocaleId.ENGLISH);
        bcs.setTargetLocale(LocaleId.FRENCH);
        bcs.handleEvent(sdEvent);
        bcs.handleEvent(tuEvent);
        logger.debug(TextUnitLogger.getTuInfo(tu, LocaleId.ENGLISH));

        assertEquals(3, BaseCounter.getCount(tu, GMX.LeveragedMatchedWordCount));
        assertEquals(0, BaseCounter.getCount(tu, GMX.FuzzyMatchedWordCount));
    }

    @Test
    public void testGMXLeveragedMatchedCharacterCountStep () {
        startupGmxTest();
        net.sf.okapi.connectors.pensieve.Parameters rparams =
                new net.sf.okapi.connectors.pensieve.Parameters();
        rparams.setDbDirectory(dbDirectory);
        tu.setSource(new TextContainer("Elephants cannot fly."));

        LeveragingStep ls = new LeveragingStep();
        ls.setSourceLocale(LocaleId.ENGLISH);
        ls.setTargetLocale(LocaleId.FRENCH);
        net.sf.okapi.steps.leveraging.Parameters params = ls.getParameters();
        params.setResourceParameters(rparams.toString());
        params.setResourceClassName(net.sf.okapi.connectors.pensieve.PensieveTMConnector.class.getName());
        params.setThreshold(99);
        params.setFillTarget(true);
        ls.handleEvent(sdEvent);
        ls.handleEvent(tuEvent);
        logger.debug(TextUnitLogger.getTuInfo(tu, LocaleId.ENGLISH));

        bcs = new GMXLeveragedMatchedCharacterCountStep();
        bcs.setSourceLocale(LocaleId.ENGLISH);
        bcs.setTargetLocale(LocaleId.FRENCH);
        bcs.handleEvent(sdEvent);
        bcs.handleEvent(tuEvent);
        logger.debug(TextUnitLogger.getTuInfo(tu, LocaleId.ENGLISH));

        assertEquals(18, BaseCounter.getCount(tu, GMX.LeveragedMatchedCharacterCount));
        assertEquals(0, BaseCounter.getCount(tu, GMX.FuzzyMatchedCharacterCount));
    }

    @Test
    public void testGMXFuzzyMatchWordCountStep () {
        startupGmxTest();
        net.sf.okapi.connectors.pensieve.Parameters rparams =
                new net.sf.okapi.connectors.pensieve.Parameters();
        rparams.setDbDirectory(dbDirectory);
        tu.setSource(new TextContainer("Elephants cannot fly here."));

        LeveragingStep ls = new LeveragingStep();
        ls.setSourceLocale(LocaleId.ENGLISH);
        ls.setTargetLocale(LocaleId.FRENCH);
        net.sf.okapi.steps.leveraging.Parameters params = ls.getParameters();
        params.setResourceParameters(rparams.toString());
        params.setResourceClassName(net.sf.okapi.connectors.pensieve.PensieveTMConnector.class.getName());
        params.setThreshold(50);
        params.setFillTarget(true);
        ls.handleEvent(sdEvent);
        ls.handleEvent(tuEvent);
        logger.debug(TextUnitLogger.getTuInfo(tu, LocaleId.ENGLISH));

        bcs = new GMXFuzzyMatchWordCountStep();
        bcs.setSourceLocale(LocaleId.ENGLISH);
        bcs.setTargetLocale(LocaleId.FRENCH);
        bcs.handleEvent(sdEvent);
        bcs.handleEvent(tuEvent);
        logger.debug(TextUnitLogger.getTuInfo(tu, LocaleId.ENGLISH));

        assertEquals(4, BaseCounter.getCount(tu, GMX.FuzzyMatchedWordCount));
        assertEquals(0, BaseCounter.getCount(tu, GMX.LeveragedMatchedWordCount));
    }

    @Test
    public void testGMXFuzzyMatchCharacterCountStep () {
        startupGmxTest();
        net.sf.okapi.connectors.pensieve.Parameters rparams =
                new net.sf.okapi.connectors.pensieve.Parameters();
        rparams.setDbDirectory(dbDirectory);
        tu.setSource(new TextContainer("Elephants cannot fly here."));

        LeveragingStep ls = new LeveragingStep();
        ls.setSourceLocale(LocaleId.ENGLISH);
        ls.setTargetLocale(LocaleId.FRENCH);
        net.sf.okapi.steps.leveraging.Parameters params = ls.getParameters();
        params.setResourceParameters(rparams.toString());
        params.setResourceClassName(net.sf.okapi.connectors.pensieve.PensieveTMConnector.class.getName());
        params.setThreshold(50);
        params.setFillTarget(true);
        ls.handleEvent(sdEvent);
        ls.handleEvent(tuEvent);
        logger.debug(TextUnitLogger.getTuInfo(tu, LocaleId.ENGLISH));

        bcs = new GMXFuzzyMatchCharacterCountStep();
        bcs.setSourceLocale(LocaleId.ENGLISH);
        bcs.setTargetLocale(LocaleId.FRENCH);
        bcs.handleEvent(sdEvent);
        bcs.handleEvent(tuEvent);
        logger.debug(TextUnitLogger.getTuInfo(tu, LocaleId.ENGLISH));

        assertEquals(22, BaseCounter.getCount(tu, GMX.FuzzyMatchedCharacterCount));
        assertEquals(0, BaseCounter.getCount(tu, GMX.LeveragedMatchedCharacterCount));
    }

    // === Test cases below are from TestCopingReport === //
    @Test
    public void testLeveraging() {

        net.sf.okapi.connectors.pensieve.Parameters params =
                new net.sf.okapi.connectors.pensieve.Parameters();
        params.setDbDirectory(dbDirectory);

        new XPipeline(
                "HTML report test",
                new XBatch(
                        new XBatchItem(root.in("test.txt").asUrl(), "UTF-8", locENUS, locESES)
                ),
                //new RawDocumentToFilterEventsStep(new HtmlFilter()),
                new RawDocumentToFilterEventsStep(new PlainTextFilter()),
                new EventLogger(),
                new XPipelineStep(
                        new SegmentationStep(),
                        //new Parameter("sourceSrxPath", pathBase + "test.srx")
                        new XParameter("sourceSrxPath", root.in("default.srx").toString()),
                        //new Parameter("sourceSrxPath", pathBase + "myRules.srx")
                        new XParameter("trimSrcLeadingWS", net.sf.okapi.steps.segmentation.Parameters.TRIM_YES),
                        new XParameter("trimSrcTrailingWS", net.sf.okapi.steps.segmentation.Parameters.TRIM_YES)
                ),
                new XPipelineStep(new LeveragingStep(),
                        new XParameter("resourceClassName", net.sf.okapi.connectors.pensieve.PensieveTMConnector.class.getName()),
                        new XParameter("resourceParameters", params.toString(), true),
                        new XParameter("threshold", 60),
                        new XParameter("fillTarget", true)
                ),
                new WordCountStep(),
                new TextUnitLogger(),
                new XPipelineStep(
                        new ScopingReportStep(),
                        new XParameter("projectName", "Test Scoping Report"),
                        new XParameter("outputPath", root.out("test_scoping_report3.html").toString())
                )
        ).execute();

//	No golden file comparison is possible as the output report contains a localized date
//		FileCompare fc = new FileCompare();
//		String outputFilePath = pathBase + "out/test_scoping_report3.html";
//		String goldFilePath = pathBase + "gold/test_scoping_report3.html";
//		assertTrue(fc.filesExactlyTheSame(outputFilePath, goldFilePath));

        logger.debug(root.out("/").toString());
    }

    @Test
    public void test_a_word_is_counted_only_once() {
        ScopingReportStep srs;
        WordCountStep wcs;
        StartDocument sd;
        Ending ed;
        Event sdEvent, edEvent;
        Event sbEvent, ebEvent;
        ITextUnit tu1, tu2, tu3, tu4;
        Event tuEvent1, tuEvent2, tuEvent3, tuEvent4;

        net.sf.okapi.connectors.pensieve.Parameters rparams =
                new net.sf.okapi.connectors.pensieve.Parameters();
        rparams.setDbDirectory(dbDirectory);

        String outputFilePath = root.out("test_scoping_report4.txt").toString();
        String goldFilePath = root.in("gold/test_scoping_report4.txt").toString();

        sbEvent = new Event(EventType.START_BATCH);
        ebEvent = new Event(EventType.END_BATCH);
        Event siEvent = new Event(EventType.START_BATCH_ITEM);
        Event eiEvent = new Event(EventType.END_BATCH_ITEM);

        sd = new StartDocument("sd");
        sd.setLocale(LocaleId.ENGLISH);
        sdEvent = new Event(EventType.START_DOCUMENT, sd);

        ed = new Ending("ed");
        edEvent = new Event(EventType.END_DOCUMENT, ed);

        tu1 = new TextUnit("tu1");
        tu1.setSource(new TextContainer("Elephants cannot fly."));
        tuEvent1 = new Event(EventType.TEXT_UNIT, tu1);

        tu2 = new TextUnit("tu2");
        tu2.setSource(new TextContainer("Elephants can't fly."));
        tuEvent2 = new Event(EventType.TEXT_UNIT, tu2);

        tu3 = new TextUnit("tu3");
        tu3.setSource(new TextContainer("Elephants can fly."));
        tuEvent3 = new Event(EventType.TEXT_UNIT, tu3);

        tu4 = new TextUnit("tu4");
        tu4.setSource(new TextContainer("Airplanes can fly."));
        tuEvent4 = new Event(EventType.TEXT_UNIT, tu4);

        LeveragingStep ls = new LeveragingStep();
        ls.setSourceLocale(LocaleId.ENGLISH);
        ls.setTargetLocale(LocaleId.FRENCH);
        net.sf.okapi.steps.leveraging.Parameters params = ls.getParameters();
        params.setResourceParameters(rparams.toString());
        params.setResourceClassName(net.sf.okapi.connectors.pensieve.PensieveTMConnector.class.getName());
        params.setThreshold(60);
        params.setFillTarget(true);

        wcs = new WordCountStep();
        srs = new ScopingReportStep();
        srs.setSourceLocale(LocaleId.ENGLISH);
        srs.setTargetLocale(LocaleId.FRENCH);
        net.sf.okapi.steps.scopingreport.Parameters params2
                = (net.sf.okapi.steps.scopingreport.Parameters) srs.getParameters();
        params2.setOutputPath(outputFilePath);
        params2.setCustomTemplateURI(root.in("golden_file_template.txt").toString());
        sd.setName(params2.getCustomTemplateURI());

        wcs.handleEvent(sbEvent);
        wcs.handleEvent(siEvent);
        wcs.handleEvent(sdEvent);
        wcs.handleEvent(tuEvent1);
        wcs.handleEvent(tuEvent2);
        wcs.handleEvent(tuEvent3);
        wcs.handleEvent(tuEvent4);
        wcs.handleEvent(edEvent);
        wcs.handleEvent(eiEvent);
        wcs.handleEvent(ebEvent);

        ls.handleEvent(sbEvent);
        ls.handleEvent(siEvent);
        ls.handleEvent(sdEvent);
        ls.handleEvent(tuEvent1);
        ls.handleEvent(tuEvent2);
        ls.handleEvent(tuEvent3);
        ls.handleEvent(tuEvent4);
        ls.handleEvent(edEvent);
        ls.handleEvent(eiEvent);
        ls.handleEvent(ebEvent);

        srs.handleEvent(sbEvent);
        srs.handleEvent(siEvent);
        srs.handleEvent(sdEvent);
        srs.handleEvent(tuEvent1);
        srs.handleEvent(tuEvent2);
        srs.handleEvent(tuEvent3);
        srs.handleEvent(tuEvent4);
        srs.handleEvent(edEvent);
        srs.handleEvent(eiEvent);
        srs.handleEvent(ebEvent);

        logger.debug(TextUnitLogger.getTuInfo(tu1, LocaleId.ENGLISH));
        logger.debug(TextUnitLogger.getTuInfo(tu2, LocaleId.ENGLISH));
        logger.debug(TextUnitLogger.getTuInfo(tu3, LocaleId.ENGLISH));
        logger.debug(TextUnitLogger.getTuInfo(tu4, LocaleId.ENGLISH));

        testPath(outputFilePath);

        FileCompare fc = new FileCompare();
        assertTrue(fc.compareFilesPerLines(outputFilePath, goldFilePath, "ISO-8859-1"));

    }

    @Test
    public void test_a_word_is_counted_only_once2() {
        ScopingReportStep srs;
        WordCountStep wcs;
        StartDocument sd;
        Ending ed;
        Event sdEvent, edEvent;
        Event sbEvent, ebEvent;
        ITextUnit tu1, tu2, tu3, tu4;
        Event tuEvent1, tuEvent2, tuEvent3, tuEvent4;

        net.sf.okapi.connectors.pensieve.Parameters rparams =
                new net.sf.okapi.connectors.pensieve.Parameters();
        rparams.setDbDirectory(dbDirectory);

        String outputFilePath = root.out("test_scoping_report5.txt").toString();
        String goldFilePath = root.in("gold/test_scoping_report5.txt").toString();

        sbEvent = new Event(EventType.START_BATCH);
        ebEvent = new Event(EventType.END_BATCH);
        Event siEvent = new Event(EventType.START_BATCH_ITEM);
        Event eiEvent = new Event(EventType.END_BATCH_ITEM);

        sd = new StartDocument("sd");
        sd.setLocale(LocaleId.ENGLISH);
        sdEvent = new Event(EventType.START_DOCUMENT, sd);

        ed = new Ending("ed");
        edEvent = new Event(EventType.END_DOCUMENT, ed);

        tu1 = new TextUnit("tu1");
        tu1.setSource(new TextContainer("Elephants cannot fly."));
        tuEvent1 = new Event(EventType.TEXT_UNIT, tu1);

        tu2 = new TextUnit("tu2");
        tu2.setSource(new TextContainer("Elephants can't fly."));
        tuEvent2 = new Event(EventType.TEXT_UNIT, tu2);

        tu3 = new TextUnit("tu3");
        tu3.setSource(new TextContainer("Elephants can fly."));
        tuEvent3 = new Event(EventType.TEXT_UNIT, tu3);

        tu4 = new TextUnit("tu4");
        tu4.setSource(new TextContainer("Airplanes can fly."));
        tuEvent4 = new Event(EventType.TEXT_UNIT, tu4);

        LeveragingStep ls = new LeveragingStep();
        ls.setSourceLocale(LocaleId.ENGLISH);
        ls.setTargetLocale(LocaleId.FRENCH);
        net.sf.okapi.steps.leveraging.Parameters params = ls.getParameters();
        params.setResourceParameters(rparams.toString());
        params.setResourceClassName(net.sf.okapi.connectors.pensieve.PensieveTMConnector.class.getName());
        params.setThreshold(60);
        params.setFillTarget(true);

        wcs = new WordCountStep();
        srs = new ScopingReportStep();
        srs.setSourceLocale(LocaleId.ENGLISH);
        srs.setTargetLocale(LocaleId.FRENCH);
        wcs.setTargetLocale(LocaleId.FRENCH);
        net.sf.okapi.steps.scopingreport.Parameters params2 = ( net.sf.okapi.steps.scopingreport.Parameters) srs.getParameters();
        params2.setOutputPath(outputFilePath);
        params2.setCustomTemplateURI(root.in("golden_file_template.txt").toString());
        params2.setCountAsNonTranslatable_ExactMatch(true);
        params2.setCountAsNonTranslatable_GMXFuzzyMatch(true);
        sd.setName(params2.getCustomTemplateURI());

        wcs.handleEvent(sbEvent);
        wcs.handleEvent(siEvent);
        wcs.handleEvent(sdEvent);
        wcs.handleEvent(tuEvent1);
        wcs.handleEvent(tuEvent2);
        wcs.handleEvent(tuEvent3);
        wcs.handleEvent(tuEvent4);
        wcs.handleEvent(edEvent);
        wcs.handleEvent(eiEvent);
        wcs.handleEvent(ebEvent);

        ls.handleEvent(sbEvent);
        ls.handleEvent(siEvent);
        ls.handleEvent(sdEvent);
        ls.handleEvent(tuEvent1);
        ls.handleEvent(tuEvent2);
        ls.handleEvent(tuEvent3);
        ls.handleEvent(tuEvent4);
        ls.handleEvent(edEvent);
        ls.handleEvent(eiEvent);
        ls.handleEvent(ebEvent);

        srs.handleEvent(sbEvent);
        srs.handleEvent(siEvent);
        srs.handleEvent(sdEvent);
        srs.handleEvent(tuEvent1);
        srs.handleEvent(tuEvent2);
        srs.handleEvent(tuEvent3);
        srs.handleEvent(tuEvent4);
        srs.handleEvent(edEvent);
        srs.handleEvent(eiEvent);
        srs.handleEvent(ebEvent);

        logger.debug(TextUnitLogger.getTuInfo(tu1, LocaleId.ENGLISH));
        logger.debug(TextUnitLogger.getTuInfo(tu2, LocaleId.ENGLISH));
        logger.debug(TextUnitLogger.getTuInfo(tu3, LocaleId.ENGLISH));
        logger.debug(TextUnitLogger.getTuInfo(tu4, LocaleId.ENGLISH));

        testPath(outputFilePath);

        FileCompare fc = new FileCompare();
        assertTrue(fc.compareFilesPerLines(outputFilePath, goldFilePath, "ISO-8859-1"));
    }

    // === Test cases below are from TestFields === //
    private void startupFieldsTest() {
        ScopingReportStep srs;
        WordCountStep wcs;
        CharacterCountStep ccs;
        StartDocument sd;
        Ending ed;
        Event sdEvent, edEvent;
        Event sbEvent, ebEvent;
        ITextUnit tu1, tu2, tu3, tu4;
        Event tuEvent1, tuEvent2, tuEvent3, tuEvent4;
        Logger localLogger = LoggerFactory.getLogger(getClass());
        root = FileLocation.fromClass(getClass());

        net.sf.okapi.connectors.pensieve.Parameters rparams =
                new net.sf.okapi.connectors.pensieve.Parameters();
        rparams.setDbDirectory(dbDirectory);

        String outputFilePath = root.out("test_scoping_report6.txt").toString();

        sbEvent = new Event(EventType.START_BATCH);
        ebEvent = new Event(EventType.END_BATCH);
        Event siEvent = new Event(EventType.START_BATCH_ITEM);
        Event eiEvent = new Event(EventType.END_BATCH_ITEM);

        sd = new StartDocument("sd");
        sd.setLocale(LocaleId.ENGLISH);
        sdEvent = new Event(EventType.START_DOCUMENT, sd);

        ed = new Ending("ed");
        edEvent = new Event(EventType.END_DOCUMENT, ed);

        tu1 = new TextUnit("tu1");
        tu1.setSource(new TextContainer("Elephants cannot fly."));
        tuEvent1 = new Event(EventType.TEXT_UNIT, tu1);

        tu2 = new TextUnit("tu2");
        tu2.setSource(new TextContainer("Elephants can't fly."));
        tuEvent2 = new Event(EventType.TEXT_UNIT, tu2);

        tu3 = new TextUnit("tu3");
        tu3.setSource(new TextContainer("Elephants can fly."));
        tuEvent3 = new Event(EventType.TEXT_UNIT, tu3);

        tu4 = new TextUnit("tu4");
        tu4.setSource(new TextContainer("Airplanes can fly."));
        tuEvent4 = new Event(EventType.TEXT_UNIT, tu4);

        LeveragingStep ls = new LeveragingStep();
        ls.setSourceLocale(LocaleId.ENGLISH);
        ls.setTargetLocale(LocaleId.FRENCH);
        net.sf.okapi.steps.leveraging.Parameters params = ls.getParameters();
        params.setResourceParameters(rparams.toString());
        params.setResourceClassName(net.sf.okapi.connectors.pensieve.PensieveTMConnector.class.getName());
        params.setThreshold(60);
        params.setFillTarget(true);

        wcs = new WordCountStep();
        ccs = new CharacterCountStep();
        srs = new ScopingReportStep();
        srs.setSourceLocale(LocaleId.ENGLISH);
        srs.setTargetLocale(LocaleId.FRENCH);
        Parameters params2 = (Parameters) srs.getParameters();
        params2.setProjectName("test_project");
        params2.setOutputPath(outputFilePath);
        params2.setCustomTemplateURI(root.in("golden_file_template2.txt").toString());
        params2.setCountAsNonTranslatable_ExactMatch(true);
        params2.setCountAsNonTranslatable_GMXFuzzyMatch(true);
        sd.setName(params2.getCustomTemplateURI());

        wcs.handleEvent(sbEvent);
        wcs.handleEvent(siEvent);
        wcs.handleEvent(sdEvent);
        wcs.handleEvent(tuEvent1);
        wcs.handleEvent(tuEvent2);
        wcs.handleEvent(tuEvent3);
        wcs.handleEvent(tuEvent4);
        wcs.handleEvent(edEvent);
        wcs.handleEvent(eiEvent);
        wcs.handleEvent(ebEvent);

        ccs.handleEvent(sbEvent);
        ccs.handleEvent(siEvent);
        ccs.handleEvent(sdEvent);
        ccs.handleEvent(tuEvent1);
        ccs.handleEvent(tuEvent2);
        ccs.handleEvent(tuEvent3);
        ccs.handleEvent(tuEvent4);
        ccs.handleEvent(edEvent);
        ccs.handleEvent(eiEvent);
        ccs.handleEvent(ebEvent);

        ls.handleEvent(sbEvent);
        ls.handleEvent(siEvent);
        ls.handleEvent(sdEvent);
        ls.handleEvent(tuEvent1);
        ls.handleEvent(tuEvent2);
        ls.handleEvent(tuEvent3);
        ls.handleEvent(tuEvent4);
        ls.handleEvent(edEvent);
        ls.handleEvent(eiEvent);
        ls.handleEvent(ebEvent);

        srs.handleEvent(sbEvent);
        srs.handleEvent(siEvent);
        srs.handleEvent(sdEvent);
        srs.handleEvent(tuEvent1);
        srs.handleEvent(tuEvent2);
        srs.handleEvent(tuEvent3);
        srs.handleEvent(tuEvent4);
        srs.handleEvent(edEvent);
        srs.handleEvent(eiEvent);
        srs.handleEvent(ebEvent);

        localLogger.debug(TextUnitLogger.getTuInfo(tu1, LocaleId.ENGLISH));
        localLogger.debug(TextUnitLogger.getTuInfo(tu2, LocaleId.ENGLISH));
        localLogger.debug(TextUnitLogger.getTuInfo(tu3, LocaleId.ENGLISH));
        localLogger.debug(TextUnitLogger.getTuInfo(tu4, LocaleId.ENGLISH));

        gen = srs.getReportGenerator();
    }

    private long getField(String fieldsName) {
        return Util.strToLong(gen.getField(fieldsName), 0L);
    }

    @Test
    public void total_counts_should_be_greater_or_equal_to_the_sum_of_categories_in_every_group() {
        startupFieldsTest();
        long totalWords = getField(ScopingReportStep.PROJECT_TOTAL_WORD_COUNT);
        long totalCharacters = getField(ScopingReportStep.PROJECT_TOTAL_CHARACTER_COUNT);

        // Okapi Words
        long count = 0;

        count += getField(ScopingReportStep.PROJECT_EXACT_UNIQUE_ID);
        count += getField(ScopingReportStep.PROJECT_EXACT_PREVIOUS_VERSION);
        count += getField(ScopingReportStep.PROJECT_EXACT_LOCAL_CONTEXT);
        count += getField(ScopingReportStep.PROJECT_EXACT_DOCUMENT_CONTEXT);
        count += getField(ScopingReportStep.PROJECT_EXACT_STRUCTURAL);
        count += getField(ScopingReportStep.PROJECT_EXACT);
        count += getField(ScopingReportStep.PROJECT_EXACT_TEXT_ONLY_PREVIOUS_VERSION);
        count += getField(ScopingReportStep.PROJECT_EXACT_TEXT_ONLY_UNIQUE_ID);
        count += getField(ScopingReportStep.PROJECT_EXACT_TEXT_ONLY);
        count += getField(ScopingReportStep.PROJECT_EXACT_REPAIRED);
        count += getField(ScopingReportStep.PROJECT_FUZZY_PREVIOUS_VERSION);
        count += getField(ScopingReportStep.PROJECT_FUZZY_UNIQUE_ID);
        count += getField(ScopingReportStep.PROJECT_FUZZY);
        count += getField(ScopingReportStep.PROJECT_FUZZY_REPAIRED);
        count += getField(ScopingReportStep.PROJECT_PHRASE_ASSEMBLED);
        count += getField(ScopingReportStep.PROJECT_MT);
        count += getField(ScopingReportStep.PROJECT_CONCORDANCE);

        assertTrue(totalWords >= count);

        // Okapi Characters
        count = 0;

        count += getField(ScopingReportStep.PROJECT_EXACT_UNIQUE_ID_CHARACTERS);
        count += getField(ScopingReportStep.PROJECT_EXACT_PREVIOUS_VERSION_CHARACTERS);
        count += getField(ScopingReportStep.PROJECT_EXACT_LOCAL_CONTEXT_CHARACTERS);
        count += getField(ScopingReportStep.PROJECT_EXACT_DOCUMENT_CONTEXT_CHARACTERS);
        count += getField(ScopingReportStep.PROJECT_EXACT_STRUCTURAL_CHARACTERS);
        count += getField(ScopingReportStep.PROJECT_EXACT_CHARACTERS);
        count += getField(ScopingReportStep.PROJECT_EXACT_TEXT_ONLY_PREVIOUS_VERSION_CHARACTERS);
        count += getField(ScopingReportStep.PROJECT_EXACT_TEXT_ONLY_UNIQUE_ID_CHARACTERS);
        count += getField(ScopingReportStep.PROJECT_EXACT_TEXT_ONLY_CHARACTERS);
        count += getField(ScopingReportStep.PROJECT_EXACT_REPAIRED_CHARACTERS);
        count += getField(ScopingReportStep.PROJECT_FUZZY_PREVIOUS_VERSION_CHARACTERS);
        count += getField(ScopingReportStep.PROJECT_FUZZY_UNIQUE_ID_CHARACTERS);
        count += getField(ScopingReportStep.PROJECT_FUZZY_CHARACTERS);
        count += getField(ScopingReportStep.PROJECT_FUZZY_REPAIRED_CHARACTERS);
        count += getField(ScopingReportStep.PROJECT_PHRASE_ASSEMBLED_CHARACTERS);
        count += getField(ScopingReportStep.PROJECT_MT_CHARACTERS);
        count += getField(ScopingReportStep.PROJECT_CONCORDANCE_CHARACTERS);

        assertTrue(totalCharacters >= count);

        // GMX Words
        count = 0;

        count += getField(ScopingReportStep.PROJECT_GMX_PROTECTED_WORD_COUNT);
        count += getField(ScopingReportStep.PROJECT_GMX_EXACT_MATCHED_WORD_COUNT);
        count += getField(ScopingReportStep.PROJECT_GMX_LEVERAGED_MATCHED_WORD_COUNT);
        count += getField(ScopingReportStep.PROJECT_GMX_REPETITION_MATCHED_WORD_COUNT);
        count += getField(ScopingReportStep.PROJECT_GMX_FUZZY_MATCHED_WORD_COUNT);
        count += getField(ScopingReportStep.PROJECT_GMX_ALPHANUMERIC_ONLY_TEXT_UNIT_WORD_COUNT);
        count += getField(ScopingReportStep.PROJECT_GMX_NUMERIC_ONLY_TEXT_UNIT_WORD_COUNT);
        count += getField(ScopingReportStep.PROJECT_GMX_MEASUREMENT_ONLY_TEXT_UNIT_WORD_COUNT);

        assertTrue(totalWords >= count);

        // GMX Words
        count = 0;

        count += getField(ScopingReportStep.PROJECT_GMX_PROTECTED_CHARACTER_COUNT);
        count += getField(ScopingReportStep.PROJECT_GMX_EXACT_MATCHED_CHARACTER_COUNT);
        count += getField(ScopingReportStep.PROJECT_GMX_LEVERAGED_MATCHED_CHARACTER_COUNT);
        count += getField(ScopingReportStep.PROJECT_GMX_REPETITION_MATCHED_CHARACTER_COUNT);
        count += getField(ScopingReportStep.PROJECT_GMX_FUZZY_MATCHED_CHARACTER_COUNT);
        count += getField(ScopingReportStep.PROJECT_GMX_ALPHANUMERIC_ONLY_TEXT_UNIT_CHARACTER_COUNT);
        count += getField(ScopingReportStep.PROJECT_GMX_NUMERIC_ONLY_TEXT_UNIT_CHARACTER_COUNT);
        count += getField(ScopingReportStep.PROJECT_GMX_MEASUREMENT_ONLY_TEXT_UNIT_CHARACTER_COUNT);

        assertTrue(totalCharacters >= count);
    }

    @Test
    public void total_counts_should_be_equal_to_the_sum_of_categories_and_nocategory() {
        startupFieldsTest();
        long totalWords = getField(ScopingReportStep.PROJECT_TOTAL_WORD_COUNT);
        long totalCharacters = getField(ScopingReportStep.PROJECT_TOTAL_CHARACTER_COUNT);

        // Okapi Words
        long count = 0;

        count += getField(ScopingReportStep.PROJECT_EXACT_UNIQUE_ID);
        count += getField(ScopingReportStep.PROJECT_EXACT_PREVIOUS_VERSION);
        count += getField(ScopingReportStep.PROJECT_EXACT_LOCAL_CONTEXT);
        count += getField(ScopingReportStep.PROJECT_EXACT_DOCUMENT_CONTEXT);
        count += getField(ScopingReportStep.PROJECT_EXACT_STRUCTURAL);
        count += getField(ScopingReportStep.PROJECT_EXACT);
        count += getField(ScopingReportStep.PROJECT_EXACT_TEXT_ONLY_PREVIOUS_VERSION);
        count += getField(ScopingReportStep.PROJECT_EXACT_TEXT_ONLY_UNIQUE_ID);
        count += getField(ScopingReportStep.PROJECT_EXACT_TEXT_ONLY);
        count += getField(ScopingReportStep.PROJECT_EXACT_REPAIRED);
        count += getField(ScopingReportStep.PROJECT_FUZZY_PREVIOUS_VERSION);
        count += getField(ScopingReportStep.PROJECT_FUZZY_UNIQUE_ID);
        count += getField(ScopingReportStep.PROJECT_FUZZY);
        count += getField(ScopingReportStep.PROJECT_FUZZY_REPAIRED);
        count += getField(ScopingReportStep.PROJECT_PHRASE_ASSEMBLED);
        count += getField(ScopingReportStep.PROJECT_MT);
        count += getField(ScopingReportStep.PROJECT_CONCORDANCE);

        assertTrue(totalWords == count + getField(ScopingReportStep.PROJECT_NOCATEGORY));

        // Okapi Characters
        count = 0;

        count += getField(ScopingReportStep.PROJECT_EXACT_UNIQUE_ID_CHARACTERS);
        count += getField(ScopingReportStep.PROJECT_EXACT_PREVIOUS_VERSION_CHARACTERS);
        count += getField(ScopingReportStep.PROJECT_EXACT_LOCAL_CONTEXT_CHARACTERS);
        count += getField(ScopingReportStep.PROJECT_EXACT_DOCUMENT_CONTEXT_CHARACTERS);
        count += getField(ScopingReportStep.PROJECT_EXACT_STRUCTURAL_CHARACTERS);
        count += getField(ScopingReportStep.PROJECT_EXACT_CHARACTERS);
        count += getField(ScopingReportStep.PROJECT_EXACT_TEXT_ONLY_PREVIOUS_VERSION_CHARACTERS);
        count += getField(ScopingReportStep.PROJECT_EXACT_TEXT_ONLY_UNIQUE_ID_CHARACTERS);
        count += getField(ScopingReportStep.PROJECT_EXACT_TEXT_ONLY_CHARACTERS);
        count += getField(ScopingReportStep.PROJECT_EXACT_REPAIRED_CHARACTERS);
        count += getField(ScopingReportStep.PROJECT_FUZZY_PREVIOUS_VERSION_CHARACTERS);
        count += getField(ScopingReportStep.PROJECT_FUZZY_UNIQUE_ID_CHARACTERS);
        count += getField(ScopingReportStep.PROJECT_FUZZY_CHARACTERS);
        count += getField(ScopingReportStep.PROJECT_FUZZY_REPAIRED_CHARACTERS);
        count += getField(ScopingReportStep.PROJECT_PHRASE_ASSEMBLED_CHARACTERS);
        count += getField(ScopingReportStep.PROJECT_MT_CHARACTERS);
        count += getField(ScopingReportStep.PROJECT_CONCORDANCE_CHARACTERS);

        assertTrue(totalCharacters == count + getField(ScopingReportStep.PROJECT_NOCATEGORY_CHARACTERS));

        // GMX Words
        count = 0;

        count += getField(ScopingReportStep.PROJECT_GMX_PROTECTED_WORD_COUNT);
        count += getField(ScopingReportStep.PROJECT_GMX_EXACT_MATCHED_WORD_COUNT);
        count += getField(ScopingReportStep.PROJECT_GMX_LEVERAGED_MATCHED_WORD_COUNT);
        count += getField(ScopingReportStep.PROJECT_GMX_REPETITION_MATCHED_WORD_COUNT);
        count += getField(ScopingReportStep.PROJECT_GMX_FUZZY_MATCHED_WORD_COUNT);
        count += getField(ScopingReportStep.PROJECT_GMX_ALPHANUMERIC_ONLY_TEXT_UNIT_WORD_COUNT);
        count += getField(ScopingReportStep.PROJECT_GMX_NUMERIC_ONLY_TEXT_UNIT_WORD_COUNT);
        count += getField(ScopingReportStep.PROJECT_GMX_MEASUREMENT_ONLY_TEXT_UNIT_WORD_COUNT);

        assertTrue(totalWords == count + getField(ScopingReportStep.PROJECT_GMX_NOCATEGORY));

        // GMX Characters
        count = 0;

        count += getField(ScopingReportStep.PROJECT_GMX_PROTECTED_CHARACTER_COUNT);
        count += getField(ScopingReportStep.PROJECT_GMX_EXACT_MATCHED_CHARACTER_COUNT);
        count += getField(ScopingReportStep.PROJECT_GMX_LEVERAGED_MATCHED_CHARACTER_COUNT);
        count += getField(ScopingReportStep.PROJECT_GMX_REPETITION_MATCHED_CHARACTER_COUNT);
        count += getField(ScopingReportStep.PROJECT_GMX_FUZZY_MATCHED_CHARACTER_COUNT);
        count += getField(ScopingReportStep.PROJECT_GMX_ALPHANUMERIC_ONLY_TEXT_UNIT_CHARACTER_COUNT);
        count += getField(ScopingReportStep.PROJECT_GMX_NUMERIC_ONLY_TEXT_UNIT_CHARACTER_COUNT);
        count += getField(ScopingReportStep.PROJECT_GMX_MEASUREMENT_ONLY_TEXT_UNIT_CHARACTER_COUNT);

        assertTrue(totalCharacters == count + getField(ScopingReportStep.PROJECT_GMX_NOCATEGORY_CHARACTERS));
    }

    @Test
    public void total_counts_should_be_equal_to_the_sum_of_translatable_and_nontranslatable() {
        startupFieldsTest();
        long total = getField(ScopingReportStep.PROJECT_TOTAL_WORD_COUNT);

        assertTrue(total == getField(ScopingReportStep.PROJECT_TRANSLATABLE_WORD_COUNT) +
                getField(ScopingReportStep.PROJECT_NONTRANSLATABLE_WORD_COUNT));

        assertTrue(total == getField(ScopingReportStep.PROJECT_GMX_TRANSLATABLE_WORD_COUNT) +
                getField(ScopingReportStep.PROJECT_GMX_NONTRANSLATABLE_WORD_COUNT));

        total = getField(ScopingReportStep.PROJECT_TOTAL_CHARACTER_COUNT);

        assertTrue(total == getField(ScopingReportStep.PROJECT_TRANSLATABLE_CHARACTER_COUNT) +
                getField(ScopingReportStep.PROJECT_NONTRANSLATABLE_CHARACTER_COUNT));

        assertTrue(total == getField(ScopingReportStep.PROJECT_GMX_TRANSLATABLE_CHARACTER_COUNT) +
                getField(ScopingReportStep.PROJECT_GMX_NONTRANSLATABLE_CHARACTER_COUNT));
    }


    private void testValue(String name, String value) {
        assertTrue(!gen.getField(name).contains(name)); // not [?FIELD_NAME]
        assertEquals(value, gen.getField(name));
    }

    private void testValue(String name, long value) {
        assertTrue(!gen.getField(name).contains(name)); // not [?FIELD_NAME]
        assertEquals(value, getField(name));
    }

    @Test
    public void testFields() {
        startupFieldsTest();
        testValue(ScopingReportStep.PROJECT_NAME, "test_project");
        //testNotEmpty(ScopingReportStep.PROJECT_DATE);
        testValue(ScopingReportStep.PROJECT_SOURCE_LOCALE, "en");
        testValue(ScopingReportStep.PROJECT_TARGET_LOCALE, "fr");
        testValue(ScopingReportStep.PROJECT_TOTAL_WORD_COUNT, 12L);
        //testNotEmpty(ScopingReportStep.ITEM_NAME);
        testValue(ScopingReportStep.ITEM_SOURCE_LOCALE, "en");
        testValue(ScopingReportStep.ITEM_TARGET_LOCALE, "fr");
        testValue(ScopingReportStep.ITEM_TOTAL_WORD_COUNT, 12L);
        testValue(ScopingReportStep.PROJECT_GMX_PROTECTED_WORD_COUNT, 0L);
        testValue(ScopingReportStep.PROJECT_GMX_EXACT_MATCHED_WORD_COUNT, 0L);
        testValue(ScopingReportStep.PROJECT_GMX_LEVERAGED_MATCHED_WORD_COUNT, 3L);
        testValue(ScopingReportStep.PROJECT_GMX_REPETITION_MATCHED_WORD_COUNT, 0L);
        testValue(ScopingReportStep.PROJECT_GMX_FUZZY_MATCHED_WORD_COUNT, 6L);
        testValue(ScopingReportStep.PROJECT_GMX_ALPHANUMERIC_ONLY_TEXT_UNIT_WORD_COUNT, 0L);
        testValue(ScopingReportStep.PROJECT_GMX_NUMERIC_ONLY_TEXT_UNIT_WORD_COUNT, 0L);
        testValue(ScopingReportStep.PROJECT_GMX_MEASUREMENT_ONLY_TEXT_UNIT_WORD_COUNT, 0L);
        testValue(ScopingReportStep.PROJECT_GMX_NONTRANSLATABLE_WORD_COUNT, 6L);
        testValue(ScopingReportStep.PROJECT_GMX_TRANSLATABLE_WORD_COUNT, 6L);
        testValue(ScopingReportStep.PROJECT_GMX_NOCATEGORY, 3L);
        testValue(ScopingReportStep.ITEM_GMX_PROTECTED_WORD_COUNT, 0L);
        testValue(ScopingReportStep.ITEM_GMX_EXACT_MATCHED_WORD_COUNT, 0L);
        testValue(ScopingReportStep.ITEM_GMX_LEVERAGED_MATCHED_WORD_COUNT, 3L);
        testValue(ScopingReportStep.ITEM_GMX_REPETITION_MATCHED_WORD_COUNT, 0L);
        testValue(ScopingReportStep.ITEM_GMX_FUZZY_MATCHED_WORD_COUNT, 6L);
        testValue(ScopingReportStep.ITEM_GMX_ALPHANUMERIC_ONLY_TEXT_UNIT_WORD_COUNT, 0L);
        testValue(ScopingReportStep.ITEM_GMX_NUMERIC_ONLY_TEXT_UNIT_WORD_COUNT, 0L);
        testValue(ScopingReportStep.ITEM_GMX_MEASUREMENT_ONLY_TEXT_UNIT_WORD_COUNT, 0L);
        testValue(ScopingReportStep.ITEM_GMX_NONTRANSLATABLE_WORD_COUNT, 6L);
        testValue(ScopingReportStep.ITEM_GMX_TRANSLATABLE_WORD_COUNT, 6L);
        testValue(ScopingReportStep.ITEM_GMX_NOCATEGORY, 3L);
        testValue(ScopingReportStep.PROJECT_EXACT_UNIQUE_ID, 0L);
        testValue(ScopingReportStep.PROJECT_EXACT_PREVIOUS_VERSION, 0L);
        testValue(ScopingReportStep.PROJECT_EXACT_LOCAL_CONTEXT, 0L);
        testValue(ScopingReportStep.PROJECT_EXACT_DOCUMENT_CONTEXT, 0L);
        testValue(ScopingReportStep.PROJECT_EXACT_STRUCTURAL, 0L);
        testValue(ScopingReportStep.PROJECT_EXACT, 3L);
        testValue(ScopingReportStep.PROJECT_EXACT_TEXT_ONLY_PREVIOUS_VERSION, 0L);
        testValue(ScopingReportStep.PROJECT_EXACT_TEXT_ONLY_UNIQUE_ID, 0L);
        testValue(ScopingReportStep.PROJECT_EXACT_TEXT_ONLY, 0L);
        testValue(ScopingReportStep.PROJECT_EXACT_REPAIRED, 0L);
        testValue(ScopingReportStep.PROJECT_FUZZY_PREVIOUS_VERSION, 0L);
        testValue(ScopingReportStep.PROJECT_FUZZY_UNIQUE_ID, 0L);
        testValue(ScopingReportStep.PROJECT_FUZZY, 6L);
        testValue(ScopingReportStep.PROJECT_FUZZY_REPAIRED, 0L);
        testValue(ScopingReportStep.PROJECT_PHRASE_ASSEMBLED, 0L);
        testValue(ScopingReportStep.PROJECT_MT, 0L);
        testValue(ScopingReportStep.PROJECT_CONCORDANCE, 0L);
        testValue(ScopingReportStep.PROJECT_NONTRANSLATABLE_WORD_COUNT, 3L);
        testValue(ScopingReportStep.PROJECT_TRANSLATABLE_WORD_COUNT, 9L);
        testValue(ScopingReportStep.PROJECT_NOCATEGORY, 3L);
        testValue(ScopingReportStep.ITEM_EXACT_UNIQUE_ID, 0L);
        testValue(ScopingReportStep.ITEM_EXACT_PREVIOUS_VERSION, 0L);
        testValue(ScopingReportStep.ITEM_EXACT_LOCAL_CONTEXT, 0L);
        testValue(ScopingReportStep.ITEM_EXACT_DOCUMENT_CONTEXT, 0L);
        testValue(ScopingReportStep.ITEM_EXACT_STRUCTURAL, 0L);
        testValue(ScopingReportStep.ITEM_EXACT, 3L);
        testValue(ScopingReportStep.ITEM_EXACT_TEXT_ONLY_PREVIOUS_VERSION, 0L);
        testValue(ScopingReportStep.ITEM_EXACT_TEXT_ONLY_UNIQUE_ID, 0L);
        testValue(ScopingReportStep.ITEM_EXACT_TEXT_ONLY, 0L);
        testValue(ScopingReportStep.ITEM_EXACT_REPAIRED, 0L);
        testValue(ScopingReportStep.ITEM_FUZZY_PREVIOUS_VERSION, 0L);
        testValue(ScopingReportStep.ITEM_FUZZY_UNIQUE_ID, 0L);
        testValue(ScopingReportStep.ITEM_FUZZY, 6L);
        testValue(ScopingReportStep.ITEM_FUZZY_REPAIRED, 0L);
        testValue(ScopingReportStep.ITEM_PHRASE_ASSEMBLED, 0L);
        testValue(ScopingReportStep.ITEM_MT, 0L);
        testValue(ScopingReportStep.ITEM_CONCORDANCE, 0L);
        testValue(ScopingReportStep.ITEM_NONTRANSLATABLE_WORD_COUNT, 3L);
        testValue(ScopingReportStep.ITEM_TRANSLATABLE_WORD_COUNT, 9L);
        testValue(ScopingReportStep.ITEM_NOCATEGORY, 3L);

        testValue(ScopingReportStep.PROJECT_TOTAL_CHARACTER_COUNT, 65L);
        testValue(ScopingReportStep.PROJECT_WHITESPACE_CHARACTER_COUNT, 8L);
        testValue(ScopingReportStep.PROJECT_PUNCTUATION_CHARACTER_COUNT, 4L);
        testValue(ScopingReportStep.PROJECT_OVERALL_CHARACTER_COUNT, 77L);
        testValue(ScopingReportStep.ITEM_TOTAL_CHARACTER_COUNT, 65L);
        testValue(ScopingReportStep.ITEM_WHITESPACE_CHARACTER_COUNT, 8L);
        testValue(ScopingReportStep.ITEM_PUNCTUATION_CHARACTER_COUNT, 4L);
        testValue(ScopingReportStep.ITEM_OVERALL_CHARACTER_COUNT, 77L);
        testValue(ScopingReportStep.PROJECT_GMX_PROTECTED_CHARACTER_COUNT, 0L);
        testValue(ScopingReportStep.PROJECT_GMX_EXACT_MATCHED_CHARACTER_COUNT, 0L);
        testValue(ScopingReportStep.PROJECT_GMX_LEVERAGED_MATCHED_CHARACTER_COUNT, 18L);
        testValue(ScopingReportStep.PROJECT_GMX_REPETITION_MATCHED_CHARACTER_COUNT, 0L);
        testValue(ScopingReportStep.PROJECT_GMX_FUZZY_MATCHED_CHARACTER_COUNT, 32L);
        testValue(ScopingReportStep.PROJECT_GMX_ALPHANUMERIC_ONLY_TEXT_UNIT_CHARACTER_COUNT, 0L);
        testValue(ScopingReportStep.PROJECT_GMX_NUMERIC_ONLY_TEXT_UNIT_CHARACTER_COUNT, 0L);
        testValue(ScopingReportStep.PROJECT_GMX_MEASUREMENT_ONLY_TEXT_UNIT_CHARACTER_COUNT, 0L);
        testValue(ScopingReportStep.PROJECT_GMX_NONTRANSLATABLE_CHARACTER_COUNT, 32L);
        testValue(ScopingReportStep.PROJECT_GMX_TRANSLATABLE_CHARACTER_COUNT, 33L);
        testValue(ScopingReportStep.PROJECT_GMX_NOCATEGORY_CHARACTERS, 15L);
        testValue(ScopingReportStep.ITEM_GMX_PROTECTED_CHARACTER_COUNT, 0L);
        testValue(ScopingReportStep.ITEM_GMX_EXACT_MATCHED_CHARACTER_COUNT, 0L);
        testValue(ScopingReportStep.ITEM_GMX_LEVERAGED_MATCHED_CHARACTER_COUNT, 18L);
        testValue(ScopingReportStep.ITEM_GMX_REPETITION_MATCHED_CHARACTER_COUNT, 0L);
        testValue(ScopingReportStep.ITEM_GMX_FUZZY_MATCHED_CHARACTER_COUNT, 32L);
        testValue(ScopingReportStep.ITEM_GMX_ALPHANUMERIC_ONLY_TEXT_UNIT_CHARACTER_COUNT, 0L);
        testValue(ScopingReportStep.ITEM_GMX_NUMERIC_ONLY_TEXT_UNIT_CHARACTER_COUNT, 0L);
        testValue(ScopingReportStep.ITEM_GMX_MEASUREMENT_ONLY_TEXT_UNIT_CHARACTER_COUNT, 0L);
        testValue(ScopingReportStep.ITEM_GMX_NONTRANSLATABLE_CHARACTER_COUNT, 32L);
        testValue(ScopingReportStep.ITEM_GMX_TRANSLATABLE_CHARACTER_COUNT, 33L);
        testValue(ScopingReportStep.ITEM_GMX_NOCATEGORY_CHARACTERS, 15L);
        testValue(ScopingReportStep.PROJECT_EXACT_UNIQUE_ID_CHARACTERS, 0L);
        testValue(ScopingReportStep.PROJECT_EXACT_PREVIOUS_VERSION_CHARACTERS, 0L);
        testValue(ScopingReportStep.PROJECT_EXACT_LOCAL_CONTEXT_CHARACTERS, 0L);
        testValue(ScopingReportStep.PROJECT_EXACT_DOCUMENT_CONTEXT_CHARACTERS, 0L);
        testValue(ScopingReportStep.PROJECT_EXACT_STRUCTURAL_CHARACTERS, 0L);
        testValue(ScopingReportStep.PROJECT_EXACT_CHARACTERS, 18L);
        testValue(ScopingReportStep.PROJECT_EXACT_TEXT_ONLY_PREVIOUS_VERSION_CHARACTERS, 0L);
        testValue(ScopingReportStep.PROJECT_EXACT_TEXT_ONLY_UNIQUE_ID_CHARACTERS, 0L);
        testValue(ScopingReportStep.PROJECT_EXACT_TEXT_ONLY_CHARACTERS, 0L);
        testValue(ScopingReportStep.PROJECT_EXACT_REPAIRED_CHARACTERS, 0L);
        testValue(ScopingReportStep.PROJECT_FUZZY_PREVIOUS_VERSION_CHARACTERS, 0L);
        testValue(ScopingReportStep.PROJECT_FUZZY_UNIQUE_ID_CHARACTERS, 0L);
        testValue(ScopingReportStep.PROJECT_FUZZY_CHARACTERS, 32L);
        testValue(ScopingReportStep.PROJECT_FUZZY_REPAIRED_CHARACTERS, 0L);
        testValue(ScopingReportStep.PROJECT_PHRASE_ASSEMBLED_CHARACTERS, 0L);
        testValue(ScopingReportStep.PROJECT_MT_CHARACTERS, 0L);
        testValue(ScopingReportStep.PROJECT_CONCORDANCE_CHARACTERS, 0L);
        testValue(ScopingReportStep.PROJECT_NONTRANSLATABLE_CHARACTER_COUNT, 18L);
        testValue(ScopingReportStep.PROJECT_TRANSLATABLE_CHARACTER_COUNT, 47L);
        testValue(ScopingReportStep.PROJECT_NOCATEGORY_CHARACTERS, 15L);
        testValue(ScopingReportStep.ITEM_EXACT_UNIQUE_ID_CHARACTERS, 0L);
        testValue(ScopingReportStep.ITEM_EXACT_PREVIOUS_VERSION_CHARACTERS, 0L);
        testValue(ScopingReportStep.ITEM_EXACT_LOCAL_CONTEXT_CHARACTERS, 0L);
        testValue(ScopingReportStep.ITEM_EXACT_DOCUMENT_CONTEXT_CHARACTERS, 0L);
        testValue(ScopingReportStep.ITEM_EXACT_STRUCTURAL_CHARACTERS, 0L);
        testValue(ScopingReportStep.ITEM_EXACT_CHARACTERS, 18L);
        testValue(ScopingReportStep.ITEM_EXACT_TEXT_ONLY_PREVIOUS_VERSION_CHARACTERS, 0L);
        testValue(ScopingReportStep.ITEM_EXACT_TEXT_ONLY_UNIQUE_ID_CHARACTERS, 0L);
        testValue(ScopingReportStep.ITEM_EXACT_TEXT_ONLY_CHARACTERS, 0L);
        testValue(ScopingReportStep.ITEM_EXACT_REPAIRED_CHARACTERS, 0L);
        testValue(ScopingReportStep.ITEM_FUZZY_PREVIOUS_VERSION_CHARACTERS, 0L);
        testValue(ScopingReportStep.ITEM_FUZZY_UNIQUE_ID_CHARACTERS, 0L);
        testValue(ScopingReportStep.ITEM_FUZZY_CHARACTERS, 32L);
        testValue(ScopingReportStep.ITEM_FUZZY_REPAIRED_CHARACTERS, 0L);
        testValue(ScopingReportStep.ITEM_PHRASE_ASSEMBLED_CHARACTERS, 0L);
        testValue(ScopingReportStep.ITEM_MT_CHARACTERS, 0L);
        testValue(ScopingReportStep.ITEM_CONCORDANCE_CHARACTERS, 0L);
        testValue(ScopingReportStep.ITEM_NONTRANSLATABLE_CHARACTER_COUNT, 18L);
        testValue(ScopingReportStep.ITEM_TRANSLATABLE_CHARACTER_COUNT, 47L);
        testValue(ScopingReportStep.ITEM_NOCATEGORY_CHARACTERS, 15L);
    }
}
