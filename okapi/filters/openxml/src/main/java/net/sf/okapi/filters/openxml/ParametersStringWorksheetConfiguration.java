/*
 * =============================================================================
 * Copyright (C) 2010-2021 by the Okapi Framework contributors
 * -----------------------------------------------------------------------------
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =============================================================================
 */
package net.sf.okapi.filters.openxml;

import net.sf.okapi.common.ParametersString;

import java.util.Arrays;
import java.util.Set;
import java.util.stream.Collectors;

final class ParametersStringWorksheetConfiguration implements WorksheetConfiguration {
    static final String DELIMITER = ",";
    private static final String DELIMITING_EXPRESSION = "\\s*" + DELIMITER + "\\s*";
    static final String NAME_PATTERN = "namePattern";
    static final String EXCLUDED_ROWS = "excludedRows";
    static final String EXCLUDED_COLUMNS = "excludedColumns";
    static final String METADATA_ROWS = "metadataRows";
    static final String METADATA_COLUMNS = "metadataColumns";
    private static final String EMPTY = "";
    private static final String DEFAULT_NAME_PATTERN = EMPTY;
    private static final String DEFAULT_EXCLUDED_ROWS = EMPTY;
    private static final String DEFAULT_EXCLUDED_COLUMNS = EMPTY;
    private static final String DEFAULT_METADATA_ROWS = EMPTY;
    private static final String DEFAULT_METADATA_COLUMNS = EMPTY;

    private final ParametersString parametersString;

    private WorksheetConfiguration worksheetConfiguration;
    private boolean read;

    ParametersStringWorksheetConfiguration(final ParametersString parametersString) {
        this.parametersString = parametersString;
    }

    @Override
    public boolean matches(final String worksheetName) {
        if (!this.read) {
            fromParametersString();
        }
        return this.worksheetConfiguration.matches(worksheetName);
    }

    @Override
    public Set<Integer> excludedRows() {
        if (!this.read) {
            fromParametersString();
        }
        return this.worksheetConfiguration.excludedRows();
    }

    @Override
    public Set<String> excludedColumns() {
        if (!this.read) {
            fromParametersString();
        }
        return this.worksheetConfiguration.excludedColumns();
    }

    @Override
    public Set<Integer> metadataRows() {
        if (!this.read) {
            fromParametersString();
        }
        return this.worksheetConfiguration.metadataRows();
    }

    @Override
    public Set<String> metadataColumns() {
        if (!this.read) {
            fromParametersString();
        }
        return this.worksheetConfiguration.metadataColumns();
    }

    /**
     * Reads worksheet configuration values from a parameters string.
     * <p>
     * The following values format is supported:
     * [namePattern]
     * [excludedRows]
     * [excludedColumns]
     * [metadataRows]
     * [metadataColumns]
     * <p>
     * The name pattern can be any supported regular expression.
     * <p>
     * The excluded rows can be a set of integer values delimited by
     * {@link ParametersStringWorksheetConfiguration#DELIMITER}.
     * <p>
     * The excluded columns can be a set of string values delimited by
     * {@link ParametersStringWorksheetConfiguration#DELIMITER}.
     */
    private void fromParametersString() {
        this.worksheetConfiguration = new WorksheetConfiguration.Default(
            this.parametersString.getString(
                ParametersStringWorksheetConfiguration.NAME_PATTERN,
                ParametersStringWorksheetConfiguration.DEFAULT_NAME_PATTERN
            ),
            Arrays.stream(
                this.parametersString.getString(
                    ParametersStringWorksheetConfiguration.EXCLUDED_ROWS,
                    ParametersStringWorksheetConfiguration.DEFAULT_EXCLUDED_ROWS
                )
                .split(DELIMITING_EXPRESSION) // returns a non empty array for an empty string
            )
            .filter(s-> !s.isEmpty())
            .map(s -> Integer.parseUnsignedInt(s))
            .collect(Collectors.toList()),
            Arrays.stream(
                this.parametersString.getString(
                    ParametersStringWorksheetConfiguration.EXCLUDED_COLUMNS,
                    ParametersStringWorksheetConfiguration.DEFAULT_EXCLUDED_COLUMNS
                )
                .split(DELIMITING_EXPRESSION)
            )
            .filter(s -> !s.isEmpty())
            .collect(Collectors.toList()),
            Arrays.stream(
                this.parametersString.getString(
                    ParametersStringWorksheetConfiguration.METADATA_ROWS,
                    ParametersStringWorksheetConfiguration.DEFAULT_METADATA_ROWS
                )
                .split(DELIMITING_EXPRESSION)
            )
            .filter(s-> !s.isEmpty())
            .map(s -> Integer.parseUnsignedInt(s))
            .collect(Collectors.toList()),
            Arrays.stream(
                this.parametersString.getString(
                    ParametersStringWorksheetConfiguration.METADATA_COLUMNS,
                    ParametersStringWorksheetConfiguration.DEFAULT_METADATA_COLUMNS
                )
                .split(DELIMITING_EXPRESSION)
            )
            .filter(s -> !s.isEmpty())
            .collect(Collectors.toList())
        );
        this.read = true;
    }

    @Override
    public <T> T writtenTo(final Output<T> output) {
        if (!this.read) {
            fromParametersString();
        }
        return this.worksheetConfiguration.writtenTo(output);
    }
}
