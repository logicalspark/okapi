/*===========================================================================
  Copyright (C) 2016-2017 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
===========================================================================*/

package net.sf.okapi.common.resource;

import net.sf.okapi.common.Util;

import java.util.Comparator;

public class CodeComparatorOnId implements Comparator<Code> {

	@Override
	public int compare(Code c1, Code c2) {
		if (c1 == c2) {
			return 0;
		}

		if (c1 == null || c2 == null) {
			return -1;
		}

		// originalId should have priority
		if (!Util.isEmpty(c1.originalId) && c1.originalId.equals(c2.getOriginalId())) {
			return 0;
		}

		return Integer.compare(c1.getId(), c2.getId());
	}
}
