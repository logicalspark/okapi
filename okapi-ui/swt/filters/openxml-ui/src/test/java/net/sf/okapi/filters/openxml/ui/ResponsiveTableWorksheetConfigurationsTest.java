/*
 * =============================================================================
 * Copyright (C) 2010-2022 by the Okapi Framework contributors
 * -----------------------------------------------------------------------------
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =============================================================================
 */
package net.sf.okapi.filters.openxml.ui;

import net.sf.okapi.common.ui.ResponsiveTable;
import net.sf.okapi.filters.openxml.WorksheetConfiguration;
import net.sf.okapi.filters.openxml.WorksheetConfigurations;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(JUnit4.class)
public class ResponsiveTableWorksheetConfigurationsTest {
    @Test @Ignore
    public void constructed() {
        final ResourceBundle rb = ResourceBundle.getBundle("net.sf.okapi.filters.openxml.ui.ResponsiveTableWorksheetConfigurationsOutput");
        final Shell shell = new Shell();
        final ResponsiveTable rt = new ResponsiveTable.Default(
            new Table(shell, SWT.MULTI | SWT.BORDER | SWT.FULL_SELECTION),
            new Menu(shell, SWT.POP_UP),
            new Menu(shell, SWT.POP_UP)
        );
        rt.configureHeader(
            new String[] {
                rb.getString("name-pattern"),
                rb.getString("excluded-rows"),
                rb.getString("excluded-columns"),
                rb.getString("metadata-rows"),
                rb.getString("metadata-columns")
            }
        );
        rt.addRow(new String[] {});
        rt.addRow(new String[] {"Sheet1", "1,2", "A,B", "3,4", "C,D"});
        rt.addRow(new String[] {"Sheet2", "1,2", "A,B", "3,4", "C,D"});
        rt.addRow(new String[] {"Sheet3", "1,2", "", "3,4", ""});
        rt.addRow(new String[] {"Sheet4", "", "A,B", "", "C,D"});
        rt.addRow(new String[] {"Sheet5", "", "", "", ""});
        final WorksheetConfigurations wcs = new WorksheetConfigurations.Default(new LinkedList<>());
        wcs.addFrom(new ResponsiveTableWorksheetConfigurationsInput(rt));

        assertThat(wcs.excludedRowsFor("").size()).isEqualTo(0);
        assertThat(wcs.excludedColumnsFor("").size()).isEqualTo(0);
        assertThat(wcs.metadataRowsFor("").size()).isEqualTo(0);
        assertThat(wcs.metadataColumnsFor("").size()).isEqualTo(0);

        assertThat(wcs.excludedRowsFor("Sheet1").size()).isEqualTo(2);
        assertThat(wcs.excludedColumnsFor("Sheet1").size()).isEqualTo(2);
        assertThat(wcs.metadataRowsFor("Sheet1").size()).isEqualTo(2);
        assertThat(wcs.metadataColumnsFor("Sheet1").size()).isEqualTo(2);

        assertThat(wcs.excludedRowsFor("Sheet2").size()).isEqualTo(2);
        assertThat(wcs.excludedColumnsFor("Sheet2").size()).isEqualTo(2);
        assertThat(wcs.metadataRowsFor("Sheet2").size()).isEqualTo(2);
        assertThat(wcs.metadataColumnsFor("Sheet2").size()).isEqualTo(2);

        assertThat(wcs.excludedRowsFor("Sheet3").size()).isEqualTo(2);
        assertThat(wcs.excludedColumnsFor("Sheet3").size()).isEqualTo(0);
        assertThat(wcs.metadataRowsFor("Sheet3").size()).isEqualTo(2);
        assertThat(wcs.metadataColumnsFor("Sheet3").size()).isEqualTo(0);

        assertThat(wcs.excludedRowsFor("Sheet4").size()).isEqualTo(0);
        assertThat(wcs.excludedColumnsFor("Sheet4").size()).isEqualTo(2);
        assertThat(wcs.metadataRowsFor("Sheet4").size()).isEqualTo(0);
        assertThat(wcs.metadataColumnsFor("Sheet4").size()).isEqualTo(2);

        assertThat(wcs.excludedRowsFor("Sheet5").size()).isEqualTo(0);
        assertThat(wcs.excludedColumnsFor("Sheet5").size()).isEqualTo(0);
        assertThat(wcs.metadataRowsFor("Sheet5").size()).isEqualTo(0);
        assertThat(wcs.metadataColumnsFor("Sheet5").size()).isEqualTo(0);
        shell.close();
    }

    @Test @Ignore
    public void exposed() {
        final WorksheetConfigurations wcs = new WorksheetConfigurations.Default(
            new WorksheetConfiguration.Default("", Collections.emptyList(), Collections.emptyList(),Collections.emptyList(), Collections.emptyList()),
            new WorksheetConfiguration.Default("", Arrays.asList(1, 2), Arrays.asList("A", "B"), Arrays.asList(3, 4), Arrays.asList("C", "D")),
            new WorksheetConfiguration.Default("Sheet2", Arrays.asList(1, 2), Arrays.asList("A", "B"), Arrays.asList(3, 4), Arrays.asList("C", "D")),
            new WorksheetConfiguration.Default("Sheet3", Arrays.asList(1, 2), Collections.emptyList(), Arrays.asList(3, 4), Collections.emptyList()),
            new WorksheetConfiguration.Default("Sheet4", Collections.emptyList(), Arrays.asList("A", "B"), Collections.emptyList(), Arrays.asList("C", "D")),
            new WorksheetConfiguration.Default("Sheet5", Collections.emptyList(), Collections.emptyList(), Collections.emptyList(), Collections.emptyList())
        );
        final Shell shell = new Shell();
        final Table table = new Table(shell, SWT.MULTI | SWT.BORDER | SWT.FULL_SELECTION);
        final ResponsiveTable rt = wcs.writtenTo(
            new ResponsiveTableWorksheetConfigurationsOutput(
                new ResponsiveTable.Default(
                    table,
                    new Menu(shell, SWT.POP_UP),
                    new Menu(shell, SWT.POP_UP)
                )
            )
        );
        final ResourceBundle rb = ResourceBundle.getBundle("net.sf.okapi.filters.openxml.ui.ResponsiveTableWorksheetConfigurationsOutput");
        assertThat(
            tableColumnsToStringList(table.getColumns())
        ).isEqualTo(
            Arrays.asList(
                rb.getString("name-pattern"),
                rb.getString("excluded-rows"),
                rb.getString("excluded-columns"),
                rb.getString("metadata-rows"),
                rb.getString("metadata-columns")
            )
        );
        final List<List<String>> output = tableItemTextsToLists(table.getColumnCount(), rt.rows());
        assertThat(
            output
        ).isEqualTo(
            Arrays.asList(
                Arrays.asList("", "", "", "", ""),
                Arrays.asList("", "1,2", "A,B", "3,4", "C,D"),
                Arrays.asList("Sheet2", "1,2", "A,B", "3,4", "C,D"),
                Arrays.asList("Sheet3", "1,2", "", "3,4", ""),
                Arrays.asList("Sheet4", "", "A,B", "", "C,D"),
                Arrays.asList("Sheet5", "", "", "", "")
            )
        );
        shell.close();
    }

    private List<String> tableColumnsToStringList(final TableColumn[] columns) {
        return Arrays.stream(columns)
            .map(tc -> tc.getText())
            .collect(Collectors.toList());
    }

    private static List<List<String>> tableItemTextsToLists(final int columnCount, final TableItem[] items) {
        return Arrays.stream(items)
            .map(ti -> tableItemToStringList(columnCount, ti))
            .collect(Collectors.toList());
    }

    private static List<String> tableItemToStringList(final int columnCount, final TableItem ti) {
        final List<String> strings = new LinkedList<>();
        for (int i = 0; i < columnCount; i++) {
            strings.add(ti.getText(i));
        }
        return strings;
    }
}
