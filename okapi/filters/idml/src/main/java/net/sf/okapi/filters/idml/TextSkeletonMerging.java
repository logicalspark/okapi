/*
 * =============================================================================
 * Copyright (C) 2010-2022 by the Okapi Framework contributors
 * -----------------------------------------------------------------------------
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =============================================================================
 */
package net.sf.okapi.filters.idml;

import net.sf.okapi.common.ISkeleton;
import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.resource.Code;
import net.sf.okapi.common.resource.ITextUnit;
import net.sf.okapi.common.resource.Segment;
import net.sf.okapi.common.resource.TextContainer;
import net.sf.okapi.common.resource.TextFragment;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventFactory;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.XMLEvent;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Map; 

final class TextSkeletonMerging {
    private static final String UNEXPECTED_CODE = "Unexpected code";

    private final XMLEventFactory eventFactory;
    private final LocaleId targetLocale;
    private Element textElement;
    private Map<Integer, MarkupRange> codeMap;
    private StringBuilder contentTextBuilder;
    private List<XMLEvent> contentEvents;

    TextSkeletonMerging(final XMLEventFactory eventFactory, final LocaleId targetLocale) {
        this.eventFactory = eventFactory;
        this.targetLocale = targetLocale;
    }

    void performFor(final ITextUnit textUnit) {
        final TextContainer tc = null == textUnit.getTarget(this.targetLocale)
            ? textUnit.getSource()
            : textUnit.getTarget(this.targetLocale);
        final TextSkeleton ts = (TextSkeleton) textUnit.getSkeleton();
        this.textElement = ts.element();
        if (TextSkeleton.DEFAULT_ATTRIBUTE_NAME.equals(ts.attributeName())) {
            this.codeMap = ts.codeMap();
            this.contentTextBuilder = new StringBuilder();
            this.contentEvents = new LinkedList<>();
            mergeCodesIn(tc);
            updateTextElementInnerEvents();
        } else {
            final ISkeleton nts = new TextSkeleton(
                updatedTextElementWith(
                    ts.attributeName(),
                    tc.getUnSegmentedContentCopy()
                ),
                ts.codeMap(),
                ts.attributeName()
            );
            nts.setParent(ts.getParent());
            textUnit.setSkeleton(nts);
        }
    }

    private void mergeCodesIn(final TextContainer textContainer) {
        for (final Segment segment : textContainer.getSegments()) {
            mergeCodesIn(segment);
        }
    }

    private void mergeCodesIn(final Segment segment) {
        final String codedText = segment.getContent().getCodedText();
        final List<Code> codes = segment.getContent().getCodes();
        for (int i = 0; i < codedText.length(); i++) {
            final char c = codedText.charAt(i);
            if (!TextFragment.isMarker(c)) {
                addChar(c);
                continue;
            }
            int codeIndex = TextFragment.toIndex(codedText.charAt(++i));
            addCode(codes.get(codeIndex));
        }
    }

    private void addChar(final char c) {
        this.contentTextBuilder.append(c);
    }

    private void addCode(final Code code) {
        final MarkupRange markupRange = this.codeMap.get(code.getId());
        if (markupRange instanceof SpecialCharacter) {
            addSpecialCharacter((SpecialCharacter) markupRange);
        } else {
            throw new IllegalStateException(UNEXPECTED_CODE + markupRange);
        }
    }

    private void addSpecialCharacter(final SpecialCharacter specialCharacter) {
        if (!(specialCharacter instanceof SpecialCharacter.Instruction)) {
            // minifying the number of Characters events
            this.contentTextBuilder.append(specialCharacter.event().asCharacters().getData());
            return;
        }
        if (0 < this.contentTextBuilder.length()) {
            this.contentEvents.add(this.eventFactory.createCharacters(this.contentTextBuilder.toString()));
            this.contentTextBuilder = new StringBuilder();
        }
        this.contentEvents.addAll(specialCharacter.getEvents());
    }

    private void updateTextElementInnerEvents() {
        if (0 == this.contentEvents.size() && 0 == this.contentTextBuilder.length()) {
            return;
        }
        if (0 < this.contentTextBuilder.length()) {
            this.contentEvents.add(this.eventFactory.createCharacters(contentTextBuilder.toString()));
        }
        if (0 < this.contentEvents.size()) {
            this.textElement.updateInnerEventsWith(this.contentEvents);
        }
    }

    private Element updatedTextElementWith(final QName attributeName, final TextFragment textFragment) {
        final Attribute na = this.eventFactory.createAttribute(attributeName, textFragment.getText());
        final List<Attribute> attributes = new LinkedList<>();
        this.textElement.startElement().getAttributes().forEachRemaining(a -> {
            if (a.getName().equals(na.getName())) {
                attributes.add(na);
            } else {
                attributes.add(a);
            }
        });
        return new Element.Default(
            this.eventFactory.createStartElement(
                this.textElement.getName(),
                attributes.iterator(),
                Collections.emptyListIterator()
            ),
            this.textElement.innerEvents(),
            this.textElement.endElement(),
            this.eventFactory
        );
    }
}
