/*
 * =============================================================================
 * Copyright (C) 2010-2021 by the Okapi Framework contributors
 * -----------------------------------------------------------------------------
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =============================================================================
 */
package net.sf.okapi.filters.openxml;

import net.sf.okapi.common.LocaleId;

import java.util.HashMap;
import java.util.Map;

interface DispersedTranslations {
    String NAMESPACE = "namespace";
    void prepareFor(final LocaleId targetLocale);
    void add(final CrossSheetCellReference crossSheetCellReference, final String namespace, final String rawSource);
    boolean presentFor(final CrossSheetCellReference crossSheetCellReference);
    String namespaceFor(final CrossSheetCellReference crossSheetCellReference);
    boolean namespaceAndRawSourceMatch(final String namespace, final String rawSource);
    void update(final CrossSheetCellReference crossSheetCellReference, final String encodedTarget);
    String encodedTargetOrRawSourceFor(final String namespace, final String rawSource, final LocaleId locale);
    void clear();

    final class Default implements DispersedTranslations {
        private static final String TRANSLATION_IS_UNAVAILABLE = "The requested dispersed translation is unavailable: ";
        private final Map<CrossSheetCellReference, DispersedTranslation> translations;
        private LocaleId targetLocale;

        Default() {
            this(new HashMap<>());
        }

        Default(final Map<CrossSheetCellReference, DispersedTranslation> translations) {
            this.translations = translations;
        }

        @Override
        public void prepareFor(final LocaleId targetLocale) {
            this.targetLocale = targetLocale;
        }

        @Override
        public void add(
            final CrossSheetCellReference crossSheetCellReference,
            final String namespace,
            final String rawSource
        ) {
            this.translations.put(
                crossSheetCellReference,
                new DispersedTranslation.Default(namespace, rawSource)
            );
        }

        @Override
        public boolean presentFor(final CrossSheetCellReference crossSheetCellReference) {
            return this.translations.containsKey(crossSheetCellReference);
        }

        @Override
        public String namespaceFor(final CrossSheetCellReference crossSheetCellReference) {
            final DispersedTranslation dt = this.translations.get(crossSheetCellReference);
            if (null == dt) {
                throw new IllegalArgumentException("The dispersed translation is not available for: ".concat(crossSheetCellReference.toString()));
            }
            return dt.namespace();
        }

        @Override
        public boolean namespaceAndRawSourceMatch(final String namespace, final String rawSource) {
            return this.translations.values().stream().anyMatch(
                dt -> dt.namespace().equals(namespace) && dt.rawSource().equals(rawSource)
            );
        }

        @Override
        public void update(final CrossSheetCellReference crossSheetCellReference, final String encodedTarget) {
            final DispersedTranslation dt = this.translations.get(crossSheetCellReference);
            dt.prepareFor(this.targetLocale, encodedTarget);
        }

        @Override
        public String encodedTargetOrRawSourceFor(
            final String namespace,
            final String rawSource,
            final LocaleId locale
        ) {
            final DispersedTranslation dt = this.translations.values().stream()
                .filter(cdt -> cdt.namespace().equals(namespace) && cdt.rawSource().equals(rawSource))
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException(
                    Default.TRANSLATION_IS_UNAVAILABLE.concat(namespace).concat(" ").concat(rawSource))
                );
            return dt.encodedTargetOrRawSourceFor(locale);
        }

        @Override
        public void clear() {
            this.translations.clear();
        }
    }
}
