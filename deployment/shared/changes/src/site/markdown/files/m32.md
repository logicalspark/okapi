# Changes from M31 to M32

<!-- MACRO{toc} -->

## Applications

* Tikal

    * The `-nocopy` option is now respected when extracting
      using the `-seg` option. This resolves [issue #571](https://bitbucket.org/okapiframework/okapi/issues/571).

## Filters

* OpenXML Filter

    * Support for Microsoft Visio 2013+ (`.vsdx`/`.vsdm`)
      files has been added.
    * Added an option to ignore placeholder text in PowerPoint master
      and layout slides.
    * Fixed [Issue #576](https://bitbucket.org/okapiframework/okapi/issues/576): XLSX files missing certain optional style data
      could crash the filter.

* ITS/XML Filter

    * Improved support for the ITS Target Pointer data category.
      Caveats: target element/attribute must exist (empty), and must be
      after the source; inline codes are not supported yet. Addresses [issue #574](https://bitbucket.org/okapiframework/okapi/issues/574).

* HTML Filter

    * Updated the pre-defined filter configuration to allow `title`
      and `dir` on all elements as per latest HTML
      specification.

* XLIFF Filter

    * Existing `ctype` values for most inline codes are now
      parsed and stored as the type field on `Code` objects.

## Connectors

* Microsoft Translation Hub

    * **IMPORTANT: authentication changes.** The connector has been
      updated to support Azure-based Microsoft Translator subscriptions,
      and the old (DataMarket) method of authentication has been removed.
      The `clientId` and `clientSecret` parameters have
      been removed and replaced with a single parameter called `azureKey`.
      If you have an existing Microsoft Translator subscription, you must
      migrate it to Azure by April 30, 2017. For information on how to do this, see
      [this Microsoft support article](https://translatorbusiness.uservoice.com/knowledgebase/articles/1078534-microsoft-translator-on-azure).

## General

    * **IMPORTANT:** The `LocaleId` class now uses ICU's `ULocale`.
      A few methods have been deprecated and others may have a slight
      different behavior. But this gives us good BCP-47 support going
      forward.
    * Updated Windows EXEs with version 3.9 of Launch4j to solve the issue
      of not being able to start with Java 8 with updates >= u100. This
      resolves [issue #575](https://bitbucket.org/okapiframework/okapi/issues/575).
    * Added a maven profile named `reports`. It produces
      various reports that might come in handy (`clirr`, `findbugs`,
      `jdepend-maven-plugin`, `versions`). To run it: \
      `mvn install -P reports`
    * Improved the build to include the creation of a DMG file and for
      signature for Mac distribution.
    * Added a new class, `net.sf.okapi.common.filters.FilterIterable`,
      implementing `Iterable<Event>`. \
      You can now do: \
      `for (Event event : new FilterIterable(filter)) { ... process event ... }` \
      On Java 8 you can also do: \
      `new FilterIterable(filter).forEach( ... lambda or method reference processing event ... );`
