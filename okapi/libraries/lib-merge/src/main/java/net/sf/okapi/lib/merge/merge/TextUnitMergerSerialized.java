/*===========================================================================
Copyright (C) 2013 by the Okapi Framework contributors
-----------------------------------------------------------------------------
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
===========================================================================*/

package net.sf.okapi.lib.merge.merge;

import net.sf.okapi.common.IResource;
import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.MimeTypeMapper;
import net.sf.okapi.common.Range;
import net.sf.okapi.common.exceptions.OkapiMergeException;
import net.sf.okapi.common.resource.CodeMatchStrategy;
import net.sf.okapi.common.resource.CodeMatches;
import net.sf.okapi.common.resource.ISegments;
import net.sf.okapi.common.resource.ITextUnit;
import net.sf.okapi.common.resource.Property;
import net.sf.okapi.common.resource.TextFragment;
import net.sf.okapi.common.resource.TextFragmentUtil;
import net.sf.okapi.common.resource.TextPart;
import net.sf.okapi.common.resource.TextUnitUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class TextUnitMergerSerialized implements ITextUnitMerger {
	private final Logger LOGGER = LoggerFactory.getLogger(getClass());
	private LocaleId trgLoc;
	private Parameters params;

	public TextUnitMergerSerialized() {
		params = new Parameters();
	}

	@Override
	public ITextUnit mergeTargets(final ITextUnit tuFromSkel, final ITextUnit tuFromTran) {
		if (tuFromSkel == null || tuFromTran == null) {
			LOGGER.warn("Null TextUnit in TextUnitMerger.");
			return tuFromSkel;
		}

		String translatedTuId = tuFromTran.getId();

		if ( !tuFromSkel.getId().equals(tuFromTran.getId()) ) {
			final String s = String.format("Text Unit id mismatch during merger: Original id=\"%s\" target id=\"%s\"", tuFromSkel.getId(), tuFromTran.getId());
			LOGGER.error(s);
			if (params.isThrowSegmentIdException()) {
				throw new OkapiMergeException(s);
			}
		}

		// create merged TextUnit. We shouldn't modify event resources if possible
		// so we copy and clone as needed until we have a complete merged
		// text unit. We start with the default tu from the original file or skel
		// to get all skeleton, properties and annotations from the filter
		final ITextUnit mergedTextUnit = tuFromSkel.clone();

		// Check if we have a translation
		// if not we merge with empty target
		if (!tuFromTran.hasTarget(trgLoc) || tuFromTran.getTarget(trgLoc) == null) {
			LOGGER.warn("No translation found for TU id='{}'.", tuFromTran.getId());
			return mergedTextUnit;
		}

		// Process the "approved" property
		boolean isTransApproved = false;
		final Property traProp = tuFromTran.getTarget(trgLoc).getProperty(Property.APPROVED);
		if ( traProp != null ) {
			isTransApproved = traProp.getValue().equals("yes");
		}
		if ( params != null && params.isApprovedOnly() && !isTransApproved ) {
			LOGGER.warn("Item id='{}': Target is not approved. Using empty translation.", tuFromSkel.getId());
			mergedTextUnit.setTarget(trgLoc, null);
			return mergedTextUnit;
		}

		// NOTE: since we tested target for null above, no need to check for null below

		// need source from translated tu since segmentation, properties,
		// codes or annotations could have changed post filter
		// For example xliff2 filter may deepen its segmentation or
		// new properties/annotations were added
		mergedTextUnit.setSource(tuFromTran.getSource().clone());

		// clone the translated target
		mergedTextUnit.setTarget(trgLoc, tuFromTran.getTarget(trgLoc).clone());

		// update new parent reference for all ISkeleton parts
		// the above clone only performs a shallow copy for ISkeleton
		if (mergedTextUnit.getSkeleton() != null) {
			mergedTextUnit.getSkeleton().setParent(mergedTextUnit);
		}

		// we need to copy over container properties/annotations from tuFromSkel
		// as some of these may be lost during translation
		IResource.copy(tuFromSkel.getSource(), mergedTextUnit.getSource());
		IResource.copy(tuFromSkel.getTarget(trgLoc), mergedTextUnit.getTarget(trgLoc));

		// We need to preserve the original segmentation for merging
		// Some segmented formats like xliff2 allow post-segmentation
		// which will cause mismatches with the original. These must be joined.
		final boolean segmentationSupported = MimeTypeMapper.isSegmentationSupported(tuFromSkel.getMimeType());
		final boolean keepCodeIds = segmentationSupported;

		// Remember the ranges to set them back after joining, may be empty list
		// must join all segments to match codes.
		// bilingual formats like xliff should preserve their code ids as they must match with any targets
		List<Range> srcRanges = null;
		List<Range> trgRanges = null;
		if (!mergedTextUnit.getSource().contentIsOneSegment()) {
			srcRanges = mergedTextUnit.getSource().getSegments().getRanges(keepCodeIds);
		}
		if (!mergedTextUnit.getTarget(trgLoc).contentIsOneSegment()) {
			trgRanges = mergedTextUnit.getTarget(trgLoc).getSegments().getRanges(keepCodeIds);
		}

		// if there are any codes that were simplified (merged or trimmed)
		// recover the original code data now.
		// this happens in TextUnitUtil.simplifyCodesPostSegmentation
		for (final TextPart tp : mergedTextUnit.getSource().getSegments()) {
			if (TextUnitUtil.hasMergedCode(tp.text)) {
				tp.text = TextUnitUtil.expandCodes(tp.text);
			}
		}

		for (final TextPart tp : mergedTextUnit.getTarget(trgLoc).getSegments()) {
			if (TextUnitUtil.hasMergedCode(tp.text)) {
				tp.text = TextUnitUtil.expandCodes(tp.text);
			}
		}

		// join the segments together for the code transfer
		// This allows to move codes anywhere in the text unit,
		// not just each part. We do remember the ranges because
		// some formats will require to be merged with segments
		if (!mergedTextUnit.getSource().contentIsOneSegment()) {
			mergedTextUnit.getSource().getSegments().joinAll(keepCodeIds);
		}
		if (!mergedTextUnit.getTarget(trgLoc).contentIsOneSegment()) {
			mergedTextUnit.getTarget(trgLoc).getSegments().joinAll(keepCodeIds);
		}

		TextFragment from = mergedTextUnit.getSource().getFirstContent();
		TextFragment to = mergedTextUnit.getTarget(trgLoc).getFirstContent();
		CodeMatches cm = TextFragmentUtil.synchronizeCodeIds(from, to, CodeMatchStrategy.STRICT);

		// we don't need tp copy metadata as the serialized format preserves it
		if (params.isAddMissing() && (cm.hasFromMismatch() || cm.hasToMismatch())) {
			TextFragmentUtil.addMissingCodes(from, to, cm);
			// Some codes might now be isolated.
			// Rebalance so they are marked properly
			to.invalidate();
			to.balanceMarkers();
		}

		if (cm.hasFromMismatch(false) || cm.hasToMismatch(false)) {
			TextFragmentUtil.logCodeMismatchErrors(cm, from, to, translatedTuId);
			if (params.isThrowCodeException()) {
				throw new OkapiMergeException("Missing or Added codes for TU: " + translatedTuId);
			}
		}

		// Check if source/target segment count is the same
		if (mergedTextUnit.getSource().getSegments().count() != mergedTextUnit.getTarget(trgLoc).getSegments().count()) {
			LOGGER.warn("Item id='{}': Different number of source and target segments.", tuFromTran.getId());
		}

		// some mergers can't handle segmented content - otherwise we could always just keep the segmented versions
		// Re-apply segmentation ranges if the format allows segments
		if (srcRanges != null && segmentationSupported) {
			mergedTextUnit.getSource().getSegments().create(srcRanges, true,
					ISegments.MetaCopyStrategy.IDENTITY);
		}
		if (trgRanges != null && segmentationSupported) {
			mergedTextUnit.getTarget(trgLoc).getSegments().create(trgRanges, true,
					ISegments.MetaCopyStrategy.IDENTITY);
		}

		return mergedTextUnit;
	}

	@Override
	public void setTargetLocale(final LocaleId trgLoc) {
		this.trgLoc = trgLoc;
	}

	public LocaleId getTargetLocale() {
		return trgLoc;
	}

	@Override
	public Parameters getParameters() {
		return params;
	}

	@Override
	public void setParameters(final Parameters params) {
		this.params = params;
	}
}
