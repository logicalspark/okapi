/*
 * =============================================================================
 * Copyright (C) 2010-2022 by the Okapi Framework contributors
 * -----------------------------------------------------------------------------
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =============================================================================
 */
package net.sf.okapi.filters.openxml.ui;

import net.sf.okapi.common.ui.ResponsiveTable;
import net.sf.okapi.filters.openxml.WorksheetConfiguration;

import java.util.Set;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

final class ResponsiveTableWorksheetConfigurationOutput implements WorksheetConfiguration.Output<ResponsiveTable> {
    private final ResponsiveTable responsiveTable;

    ResponsiveTableWorksheetConfigurationOutput(final ResponsiveTable responsiveTable) {
        this.responsiveTable = responsiveTable;
    }

    @Override
    public ResponsiveTable writtenWith(final Pattern namePattern, final Set<Integer> excludedRows, final Set<String> excludedColumns, final Set<Integer> metadataRows, final Set<String> metadataColumns) {
        this.responsiveTable.addRow(
            new String[] {
                namePattern.toString(),
                excludedRows.stream()
                    .map(r -> Integer.toUnsignedString(r))
                    .collect(Collectors.joining(TableItemWorksheetConfiguration.DELIMITER)),
                excludedColumns.stream()
                    .collect(Collectors.joining(TableItemWorksheetConfiguration.DELIMITER)),
                metadataRows.stream()
                    .map(r -> Integer.toUnsignedString(r))
                    .collect(Collectors.joining(TableItemWorksheetConfiguration.DELIMITER)),
                metadataColumns.stream()
                    .collect(Collectors.joining(TableItemWorksheetConfiguration.DELIMITER))
            }
        );
        return this.responsiveTable;
    }
}
