package net.sf.okapi.roundtrip.integration;

import net.sf.okapi.common.integration.FileComparator;
import net.sf.okapi.common.integration.XmlOrTextRoundTripIT;
import net.sf.okapi.filters.icml.ICMLFilter;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.FileNotFoundException;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;

@RunWith(JUnit4.class)
public class RoundTripIcmlIT extends XmlOrTextRoundTripIT {
	private static final String CONFIG_ID = "okf_icml";
	private static final String DIR_NAME = "/icml/";
	private static final List<String> EXTENSIONS = Arrays.asList(".icml", ".wcml");

	public RoundTripIcmlIT() {
		super(CONFIG_ID, DIR_NAME, EXTENSIONS, ICMLFilter::new);
	}

	@Test
	public void icmlFiles() throws FileNotFoundException, URISyntaxException {
		setSerializedOutput(false);
		realTestFiles(false, new FileComparator.XmlComparator());
	}

	@Test
	public void icmlSerializedFiles() throws FileNotFoundException, URISyntaxException {
		setSerializedOutput(true);
		realTestFiles(false, new FileComparator.XmlComparator());
	}
}
