/*
 * =============================================================================
 * Copyright (C) 2010-2022 by the Okapi Framework contributors
 * -----------------------------------------------------------------------------
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =============================================================================
 */
package net.sf.okapi.filters.openxml;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

interface RevisionHeadersFragments extends Iterable<RevisionHeaderFragments> {
    void readWith(final XMLEventReader eventReader) throws XMLStreamException;

    final class Default implements RevisionHeadersFragments {
        private static final String HEADERS = "headers";
        private static final String HEADER = "header";
        private static final String ID = "id";
        private final List<RevisionHeaderFragments> headerFragments;
        private QName idName;

        Default() {
            this(new LinkedList<>());
        }

        Default(final List<RevisionHeaderFragments> headerFragments) {
            this.headerFragments = headerFragments;
        }

        @Override
        public Iterator<RevisionHeaderFragments> iterator() {
            return this.headerFragments.iterator();
        }

        @Override
        public void readWith(final XMLEventReader eventReader) throws XMLStreamException {
            while (eventReader.hasNext()) {
                final XMLEvent e = eventReader.nextEvent();
                if (!e.isStartElement()) {
                    continue;
                }
                final StartElement se = e.asStartElement();
                if (Default.HEADERS.equals(se.getName().getLocalPart())) {
                    qualifyIdName(se);
                } else if (Default.HEADER.equals(se.getName().getLocalPart())) {
                    final RevisionHeaderFragments rhf = new RevisionHeaderFragments.Default(se, this.idName);
                    rhf.readWith(eventReader);
                    this.headerFragments.add(rhf);
                }
            }
        }

        private void qualifyIdName(final StartElement startElement) {
            final String namespaceUri = startElement.getNamespaceURI(Namespace.PREFIX_R);
            if (null == namespaceUri) {
                return;
            }
            this.idName = new QName(
                namespaceUri, Default.ID, Namespace.PREFIX_R
            );
        }
    }
}
